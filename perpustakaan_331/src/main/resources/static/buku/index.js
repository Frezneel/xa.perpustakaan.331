const page = $("#page").val();
const size = $("#size").val();
const search_query = $("#search_query").val();
$("#search_input").val(search_query);

getAllBuku();
function getAllBuku(){
    const is_search = search_query == "" ? false : true;
    const url_query = is_search ? host+'/api/buku/result?search_query='+search_query+'&page='+page+'&size='+size : host+'/api/buku/map?page='+page+'&size='+size;
    $.ajax({
        url: url_query,
        type:'GET',
        contentType:'application/json',
        success:function(data){
            const halamanSekarang = data.halamanSekarang;
            const totalHalaman = data.totalHalaman;

            $("#bukuData").html(``);
            const no_data = (halamanSekarang-1) * size;
            for(i = 0; i<data.dataApi.length; i++){
                $("#bukuData").append(`
                    <tr>
                        <td>${(i+1)+no_data}</td>
                        <td>${data.dataApi[i].judul}</td>
                        <td>${data.dataApi[i].tahun_terbit}</td>
                        <td>${data.dataApi[i].jumlah_buku}</td>
                        <td>${data.dataApi[i].jumlah_halaman}</td>
                        <td>${data.dataApi[i].isbn}</td>
                        <td>${data.dataApi[i].pengarang.nama}</td>
                        <td>${data.dataApi[i].penerbit.nama}</td>
                        <td>${data.dataApi[i].rak.lokasi}</td>
                        <th>
                            <button class="btn bg-primary" onclick="show_(${data.dataApi[i].id})"><i class="bi bi-image"></i> Show</button>
                            <button class="btn bg-warning" onclick="form_(${data.dataApi[i].id})"><i class="bi bi-pencil-square"></i> Edit</button>
                            <button class="btn bg-danger" onclick="delete_(${data.dataApi[i].id})"><i class="bi bi-trash3"></i> Delete</button>
                        </th>
                    </tr>
                `)
            }
            const url_page = is_search ? host+"/buku/result?search_query="+search_query+"&" : host+"/buku?";
            $("#halamanNav").html(``);
            if(halamanSekarang != 1){
                $("#halamanNav").append(
                `
                    <li class="page-item"><a class="page-link" href="${url_page}page=${halamanSekarang-1}&size=${size}">Previous</a></li>
                `
                );
            }
            for(i = 1; i <= data.totalHalaman; i++){
                if(i == data.halamanSekarang){
                    $("#halamanNav").append(`
                        <li class="page-item disabled"><a class="page-link" href="${url_page}page=${i}&size=${size}">${i}</a></li>
                    `);
                }else{
                    $("#halamanNav").append(`
                        <li class="page-item"><a class="page-link" href="${url_page}page=${i}&size=${size}">${i}</a></li>
                    `);
                }
            }
            if(halamanSekarang != totalHalaman){
                $("#halamanNav").append(
                `
                    <li class="page-item"><a class="page-link" href="${url_page}page=${halamanSekarang+1}&size=${size}">Next</a></li>
                `
                );
            }
        }
    })
}

$("#search_button").click(function(){
    const query = ($("#search_input").val()).trim();
    if(query != ""){
        window.location = host+'/buku/result?search_query='+query;
    }
})

function form_(id){
    var str = ``;
    var pengarang;
    var penerbit;
    var rak;
    $.ajax({
        url: host + '/api/pengarang',
        type:'GET',
        contentType:'application/json',
        async:false,
        success:function(data){
            pengarang = data;
        }
    })
    $.ajax({
        url: host + '/api/penerbit',
        type:'GET',
        contentType:'application/json',
        async:false,
        success:function(data){
            penerbit = data;
        }
    })
    $.ajax({
        url: host + '/api/rak',
        type:'GET',
        contentType:'application/json',
        async:false,
        success:function(data){
            rak = data;
        }
    })
    str += `<div id="errorInfo" style="display: none;">
            </div>`;
    if(id){
        $.ajax({
            url: host + '/api/buku/' + id,
            type: "get",
            contentType:'application/json',
            async:false,
            success:function(data){
                str = `Judul : <input class="form-control" type="text" id="judul" value="${data.judul}">`;
                str += `<small class="errorField text-danger" type="text" id="errJudul"></small> <br>`;
                str += `Tahun terbit : <input class="form-control" type="text" id="tahun_terbit" value="${data.tahun_terbit}">`;
                str += `<small class="errorField text-danger" type="text" id="errTahunTerbit"></small> <br>`;
                str += `Jumlah buku : <input class="form-control" type="text" id="jumlah_buku" value="${data.jumlah_buku}">`;
                str += `<small class="errorField text-danger" type="text" id="errJumlahBuku"></small> <br>`;
                str += `Jumlah halaman : <input class="form-control" type="text" id="jumlah_halaman" value="${data.jumlah_halaman}">`;
                str += `<small class="errorField text-danger" type="text" id="errJumlahHalaman"></small> <br>`;
                str += `ISBN : <input class="form-control" type="text" id="isbn" value="${data.isbn}">`;
                str += `<small class="errorField text-danger" type="text" id="errISBN"></small> <br>`;
                str += `Pengarang : <select class="form-select" id="pengarang">`;
                for(i=0; i < pengarang.length; i++){
                   if(data.pengarang_id === pengarang[i].id){
                      str += `<option value="${pengarang[i].id}" selected>${pengarang[i].nama}</option>`;
                   }else{
                      str += `<option value="${pengarang[i].id}">${pengarang[i].nama}</option>`;
                   }
                }
                str += `</select>`;
                str += `<small class="errorField text-danger" type="text" id="errPengarang"></small> <br>`;
                str += `Penerbit : <select class="form-select" id="penerbit">`;
                for(i=0; i < penerbit.length; i++){
                    if(data.penerbit_id === penerbit[i].id){
                        str += `<option value="${penerbit[i].id}" selected>${penerbit[i].nama}</option>`;
                    }else{
                        str += `<option value="${penerbit[i].id}">${penerbit[i].nama}</option>`;
                    }
                }
                str += `</select>`;
                str += `<small class="errorField text-danger" type="text" id="errPenerbit"></small> <br>`;
                str += `Rak : <select class="form-select" id="rak">`;
                for(i=0; i < rak.length; i++){
                    if(data.rak_id === rak[i].id){
                        str += `<option value="${rak[i].id}" selected>${rak[i].lokasi}</option>`;
                    }else{
                        str += `<option value="${rak[i].id}">${rak[i].lokasi}</option>`;
                    }
                }
                str += `</select>`;
                str += `<small class="errorField text-danger" type="text" id="errRak"></small> <br>`;
                str += `<hr>
                        <button class="btn btn-warning" onclick="editBuku(${data.id})">edit</button>
                        <button class="btn btn-primary" onclick="batal()">batal</button>`;
            }
        })
    }else{
        str = `Judul : <input class="form-control" type="text" id="judul">`;
        str += `<small class="errorField text-danger" type="text" id="errJudul"></small> <br>`;
        str += `Tahun terbit : <input class="form-control" type="text" id="tahun_terbit">`;
        str += `<small class="errorField text-danger" type="text" id="errTahunTerbit"></small> <br>`;
        str += `Jumlah buku : <input class="form-control" type="text" id="jumlah_buku">`;
        str += `<small class="errorField text-danger" type="text" id="errJumlahBuku"></small> <br>`;
        str += `Jumlah halaman : <input class="form-control" type="text" id="jumlah_halaman">`;
        str += `<small class="errorField text-danger" type="text" id="errJumlahHalaman"></small> <br>`;
        str += `ISBN : <input class="form-control" type="text" id="isbn">`;
        str += `<small class="errorField text-danger" type="text" id="errISBN"></small> <br>`;
        str += `Pengarang : <select class="form-select" id="pengarang">`;
        for(i=0; i < pengarang.length; i++){
          str += `<option value="${pengarang[i].id}">${pengarang[i].nama}</option>`;
        }
        str += `</select>`;
        str += `<small class="errorField text-danger" type="text" id="errPengarang"></small> <br>`;
        str += `Penerbit : <select class="form-select" id="penerbit">`;
        for(i=0; i < penerbit.length; i++){
          str += `<option value="${penerbit[i].id}">${penerbit[i].nama}</option>`;
        }
        str += `</select>`;
        str += `<small class="errorField text-danger" type="text" id="errPenerbit"></small> <br>`;
        str += `Rak : <select class="form-select" id="rak">`;
        for(i=0; i < rak.length; i++){
          str += `<option value="${rak[i].id}">${rak[i].lokasi}</option>`;
        }
        str += `</select>`;
        str += `<small class="errorField text-danger" type="text" id="errRak"></small> <br>`;

        str += `<hr>
                <button class="btn btn-success" onclick="simpanBuku()">simpan</button>
                <button class="btn btn-primary" onclick="batal()">batal</button>`;
    }

    $(".modal-body").html(str);
    $("#mymodal").modal("show");
    $(".modal-title").html("Buku Form")
}

function simpanBuku(){
    var judul = $("#judul").val();
    var tahun_terbit = $("#tahun_terbit").val();
    var jumlah_buku = $("#jumlah_buku").val();
    var jumlah_halaman = $("#jumlah_halaman").val();
    var isbn = $("#isbn").val();
    var pengarang = $("#pengarang").val();
    var penerbit = $("#penerbit").val();
    var rak = $("#rak").val();
    var verifJudul = false;
    var verifTahunTerbit = false;
    var verifJumlahBuku = false;
    var verifJumlahHalaman = false;
    var verifISBN = false;
    var verifPengarang = false;
    var verifPenerbit = false;
    var verifRak = false;
    $("#errJudul").text("");
    $("#errTahunTerbit").text("");
    $("#errJumlahBuku").text("");
    $("#errJumlahHalaman").text("");
    $("#errISBN").text("");
    $("#errPengarang").text("");
    $("#errPenerbit").text("");
    $("#errRak").text("");

    if(judul == ""){
       $("#errJudul").text("*tidak boleh kosong");
       verifJudul = true;
    }
    if(tahun_terbit == ""){
       $("#errTahunTerbit").text("*tidak boleh kosong");
       verifTahunTerbit = true;
    }
    if(jumlah_buku == ""){
       $("#errJumlahBuku").text("*tidak boleh kosong");
       verifJumlahBuku = true;
    }
    if(jumlah_halaman == ""){
       $("#errJumlahHalaman").text("*tidak boleh kosong");
       verifJumlahHalaman = true;
    }
    if(isbn == ""){
       $("#errISBN").text("*tidak boleh kosong");
       verifISBN = true;
    }
    if(pengarang == ""){
       $("#errPengarang").text("*tidak boleh kosong");
       verifPengarang = true;
    }
    if(penerbit == ""){
       $("#errPenerbit").text("*tidak boleh kosong");
       verifPenerbit = true;
    }
    if(rak == ""){
       $("#errRak").text("*tidak boleh kosong");
       verifRak = true;
    }
    if(verifJudul || verifTahunTerbit || verifJumlahBuku || verifJumlahHalaman || verifISBN || verifPengarang
     || verifPenerbit || verifRak){
        return
    }

    const buku = {
        judul: judul,
        tahun_terbit: tahun_terbit,
        jumlah_buku: jumlah_buku,
        jumlah_halaman: jumlah_halaman,
        isbn: isbn,
        pengarang_id: pengarang,
        penerbit_id: penerbit,
        rak_id: rak
    }

    $.ajax({
        url: host + '/api/buku',
        type: "POST",
        dataType: "JSON",
        data: JSON.stringify(buku),
        contentType: "application/json",
        success: function(result){
            if(result.status == "ada"){
                appendAlert('Error: buku sudah ada', 'danger');
                $("#errorInfo").show();
            }else{
                $('#mymodal').modal('hide');
                getAllBuku();
            }
        }
    })
}

function editBuku(id){
    var judul = $("#judul").val();
    var tahun_terbit = $("#tahun_terbit").val();
    var jumlah_buku = $("#jumlah_buku").val();
    var jumlah_halaman = $("#jumlah_halaman").val();
    var isbn = $("#isbn").val();
    var pengarang = $("#pengarang").val();
    var penerbit = $("#penerbit").val();
    var rak = $("#rak").val();
    var verifJudul = false;
    var verifTahunTerbit = false;
    var verifJumlahBuku = false;
    var verifJumlahHalaman = false;
    var verifISBN = false;
    var verifPengarang = false;
    var verifPenerbit = false;
    var verifRak = false;
    $("#errJudul").text("");
    $("#errTahunTerbit").text("");
    $("#errJumlahBuku").text("");
    $("#errJumlahHalaman").text("");
    $("#errISBN").text("");
    $("#errPengarang").text("");
    $("#errPenerbit").text("");
    $("#errRak").text("");

    if(judul == ""){
       $("#errJudul").text("*tidak boleh kosong");
       verifJudul = true;
    }
    if(tahun_terbit == ""){
       $("#errTahunTerbit").text("*tidak boleh kosong");
       verifTahunTerbit = true;
    }
    if(jumlah_buku == ""){
       $("#errJumlahBuku").text("*tidak boleh kosong");
       verifJumlahBuku = true;
    }
    if(jumlah_halaman == ""){
       $("#errJumlahHalaman").text("*tidak boleh kosong");
       verifJumlahHalaman = true;
    }
    if(isbn == ""){
       $("#errISBN").text("*tidak boleh kosong");
       verifISBN = true;
    }
    if(pengarang == ""){
       $("#errPengarang").text("*tidak boleh kosong");
       verifPengarang = true;
    }
    if(penerbit == ""){
       $("#errPenerbit").text("*tidak boleh kosong");
       verifPenerbit = true;
    }
    if(rak == ""){
       $("#errRak").text("*tidak boleh kosong");
       verifRak = true;
    }
    if(verifJudul || verifTahunTerbit || verifJumlahBuku || verifJumlahHalaman || verifISBN || verifPengarang
     || verifPenerbit || verifRak){
        return
    }

    const buku = {
        judul: judul,
        tahun_terbit: tahun_terbit,
        jumlah_buku: jumlah_buku,
        jumlah_halaman: jumlah_halaman,
        isbn: isbn,
        pengarang_id: pengarang,
        penerbit_id: penerbit,
        rak_id: rak
    }

    $.ajax({
        url: host + '/api/buku/' + id,
        type: "PUT",
        dataType: "JSON",
        data: JSON.stringify(buku),
        contentType: "application/json",
        success: function(result){
            if(result.status == "ada"){
                appendAlert('Error: buku sudah ada', 'danger');
                $("#errorInfo").show();
            }else{
                $('#mymodal').modal('hide');
                getAllBuku();
            }
        }
    })
}

function batal(){
    $("#mymodal").modal("hide");
}

function delete_(id){
    var str = ``;
    str = `<h5>Apakah anda yakin ingin menghapus data ini?</h5>`;
    str += `<hr><button class="btn btn-danger" onclick="deleteData(${id})">hapus</button>
                <button class="btn btn-primary" onclick="batal()">batal</button>`;
    $(".modal-body").html(str);
    $("#mymodal").modal("show");
    $(".modal-title").html("Delete buku")
}

function deleteData(id){
    $.ajax({
        url: host + '/api/buku/' + id,
        type:'DELETE',
        contentType:'application/json',
        success:function(result){
            alert("Berhasil");
            $("#mymodal").modal("hide");
            getAllBuku();
        }
    })
}

function show_(id){
    $.ajax({
        url: host + api_v1 + 'buku/' + id,
        type:'GET',
        contentType:'application/json',
        async:false,
        success:function(data){
            if(data.image_path){
                var html_image = `<img src=` + data.image_path + ` width="100%">`;
                html_image += `<div class="d-flex justify-content-center mt-2"><button class="btn bg-secondary text-light">Show full</button></div>`;
                $('.modal-title').html("Show Picture");
                $('.modal-body').html(html_image);
                $(".modal").modal("show");
            }else{
                var html_image = `<div class="d-flex justify-content-center" id="foto"> <canvas id="canvas" class="img-fluid">`
                html_image += `<img src="" alt="file_picture" style="width=100%" id="file_picture"></canvas></div>`
                $('.modal-title').html("Show Picture");
                $('.modal-body').html(html_image);
                $(".modal").modal("show");
                viewImage(data.image_file);
            }
        }
    })
}

function pageShow(data){
    if(data.image_path){
        window.open(data.image_path);
    }else{
        alert("Empty");
    }
}

function viewImage(dataImage){
    var canvas = document.getElementById("canvas");
    var ctx = canvas.getContext("2d");
    canvas.width = 256;
    canvas.height = 256;
    var image = document.getElementById("file_picture");
    var imageData = 'data:image/png;base64,' + dataImage;
    image.onload = function(e) {
        ctx.drawImage(image,
            0, 0, image.width, image.height,
            0, 0, canvas.width, canvas.height
        );
        // create a new base64 encoding
        var resampledImage = new Image();
        resampledImage.src = canvas.toDataURL();
    };
    if(dataImage != null){
        document.getElementById("file_picture").src = imageData;
    }else{
        document.getElementById("file_picture").src = "../default_picture/not_found_image.png";
    }
}

const appendAlert = (message, type) => {
  const wrapper = document.createElement('div');
  wrapper.innerHTML = [
    `<div class="alert alert-${type} alert-dismissible" role="alert">`,
    `   <div>${message}</div>`,
    '   <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>',
    '</div>'
  ].join('');
  $("#errorInfo").html(wrapper);
}

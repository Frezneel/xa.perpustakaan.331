const page = $("#page").val();
const size = $("#size").val();
const search_query = $("#search_query").val();
$("#search_input").val(search_query);

getAllPengarang();
function getAllPengarang(){
    const is_search = search_query == "" ? false : true;
    const url_query = is_search ? host+'/api/pengarang/result?search_query='+search_query+'&page='+page+'&size='+size : host+'/api/pengarang/map?page='+page+'&size='+size;
    $.ajax({
        url: url_query,
        type:'GET',
        contentType:'application/json',
        success:function(data){
            const halamanSekarang = data.halamanSekarang;
            const totalHalaman = data.totalHalaman;

            $("#pengarangData").html(``);
            const no_data = (halamanSekarang-1) * size;
            for(i = 0; i<data.dataApi.length; i++){
                $("#pengarangData").append(`
                    <tr>
                        <td>${(i+1)+no_data}</td>
                        <td>${data.dataApi[i].nama}</td>
                        <td>${data.dataApi[i].alamat}</td>
                        <td>${data.dataApi[i].telp}</td>
                        <th>
                            <button class="btn bg-warning" onclick="form_(${data.dataApi[i].id})">Edit</button>
                            <button class="btn bg-danger" onclick="delete_(${data.dataApi[i].id})">Delete</button>
                        </th>
                    </tr>
                `)
            }
            const url_page = is_search ? host+"/pengarang/result?search_query="+search_query+"&" : host+"/pengarang?";
            $("#halamanNav").html(``);
            if(halamanSekarang != 1){
                $("#halamanNav").append(
                `
                    <li class="page-item"><a class="page-link" href="${url_page}page=${halamanSekarang-1}&size=${size}">Previous</a></li>
                `
                );
            }
            for(i = 1; i <= data.totalHalaman; i++){
                if(i == data.halamanSekarang){
                    $("#halamanNav").append(`
                        <li class="page-item disabled"><a class="page-link" href="${url_page}page=${i}&size=${size}">${i}</a></li>
                    `);
                }else{
                    $("#halamanNav").append(`
                        <li class="page-item"><a class="page-link" href="${url_page}page=${i}&size=${size}">${i}</a></li>
                    `);
                }
            }
            if(halamanSekarang != totalHalaman){
                $("#halamanNav").append(
                `
                    <li class="page-item"><a class="page-link" href="${url_page}page=${halamanSekarang+1}&size=${size}">Next</a></li>
                `
                );
            }
        }
    })
}

$("#search_button").click(function(){
    const query = ($("#search_input").val()).trim();
    if(query != ""){
        window.location = host+'/pengarang/result?search_query='+query;
    }
})

function form_(id){
    var str = ``;
    str += `<div id="errorInfo" style="display: none;">
            </div>`;
    if(id){
        $.ajax({
            url: host + '/api/pengarang/' + id,
            type: "get",
            contentType:'application/json',
            async:false,
            success:function(data){
                str += `Nama : <input class="form-control" type="text" id="nama" value="${data.nama}">`;
                str += `<small class="errorField text-danger" type="text" id="errNama"></small> <br>`;
                str += `Alamat : <input class="form-control" type="text" id="alamat" value="${data.alamat}">`;
                str += `<small class="errorField text-danger" type="text" id="errAlamat"></small> <br>`;
                str += `Telephone : <input class="form-control" type="text" id="telp" value="${data.telp}">`;
                str += `<small class="errorField text-danger" type="text" id="errTelp"></small> <br>`;
                str += `<hr>
                        <button class="btn btn-warning" onclick="editPengarang(${data.id})">edit</button>
                        <button class="btn btn-primary" onclick="batal()">batal</button>`;
            }
        })
    }else{
        str += `Nama : <input class="form-control" type="text" id="nama">`;
        str += `<small class="errorField text-danger" type="text" id="errNama"></small> <br>`;
        str += `Alamat : <input class="form-control" type="text" id="alamat">`;
        str += `<small class="errorField text-danger" type="text" id="errAlamat"></small> <br>`;
        str += `Telephone : <input class="form-control" type="text" id="telp">`;
        str += `<small class="errorField text-danger" type="text" id="errTelp"></small> <br>`;
        str += `<hr>
                <button class="btn btn-success" onclick="simpanPengarang()">simpan</button>
                <button class="btn btn-primary" onclick="batal()">batal</button>`;
    }

    $(".modal-body").html(str);
    $("#mymodal").modal("show");
    $(".modal-title").html("Pengarang Form")
}

function simpanPengarang(){
    var nama = $("#nama").val();
    var alamat = $("#alamat").val();
    var telp = $("#telp").val();
    var verifNama = false;
    var verifAlamat = false;
    var verifTelp = false;
    $("#errNama").text("");
    $("#errAlamat").text("");
    $("#errTelp").text("");

    if(nama == ""){
       $("#errNama").text("*tidak boleh kosong");
       verifNama = true;
    }
    if(alamat == ""){
       $("#errAlamat").text("*tidak boleh kosong");
       verifAlamat = true;
    }
    if(telp == ""){
       $("#errTelp").text("*tidak boleh kosong");
       verifTelp = true;
    }
    if(verifNama || verifAlamat || verifTelp){
        return
    }

    const pengarang = {
        nama: nama,
        alamat: alamat,
        telp: telp,
    }

    $.ajax({
        url: host + '/api/pengarang',
        type: "POST",
        dataType: "JSON",
        data: JSON.stringify(pengarang),
        contentType: "application/json",
        success: function(result){
            if(result.status == "ada"){
                appendAlert('Error: data sudah ada', 'danger');
                $("#errorInfo").show();
            }else{
                $('#mymodal').modal('hide');
                getAllPengarang();
            }
        }
    })

}

function editPengarang(id){
    var nama = $("#nama").val();
    var alamat = $("#alamat").val();
    var telp = $("#telp").val();
    var verifNama = false;
    var verifAlamat = false;
    var verifTelp = false;
    $("#errNama").text("");
    $("#errAlamat").text("");
    $("#errTelp").text("");

    if(nama == ""){
       $("#errNama").text("*tidak boleh kosong");
       verifNama = true;
    }
    if(alamat == ""){
       $("#errAlamat").text("*tidak boleh kosong");
       verifAlamat = true;
    }
    if(telp == ""){
       $("#errTelp").text("*tidak boleh kosong");
       verifTelp = true;
    }
    if(verifNama || verifAlamat || verifTelp){
        return
    }

    const pengarang = {
        nama: nama,
        alamat: alamat,
        telp: telp,
    }

    console.log(pengarang);

    $.ajax({
        url: host + '/api/pengarang/' + id,
        type: "PUT",
        dataType: "JSON",
        data: JSON.stringify(pengarang),
        contentType: "application/json",
        success: function(result){
            if(result.status == "ada"){
                appendAlert('Error: data sudah ada', 'danger');
                $("#errorInfo").show();
            }else{
                $('#mymodal').modal('hide');
                getAllPengarang();
            }
        }
    })

}

function batal(){
    $("#mymodal").modal("hide");
}

function delete_(id){
    var str = ``;
    str = `<h5>Apakah anda yakin ingin menghapus data ini?</h5>`;
    str += `<hr><button class="btn btn-danger" onclick="deleteData(${id})">hapus</button>
                <button class="btn btn-primary" onclick="batal()">batal</button>`;
    $(".modal-body").html(str);
    $("#mymodal").modal("show");
    $(".modal-title").html("Delete pengarang")
}

function deleteData(id){
    $.ajax({
        url: host + '/api/pengarang/' + id,
        type:'DELETE',
        contentType:'application/json',
        success:function(result){
            alert("Berhasil");
            $("#mymodal").modal("hide");
            getAllPengarang();
        }
    })
}

const appendAlert = (message, type) => {
  const wrapper = document.createElement('div');
  wrapper.innerHTML = [
    `<div class="alert alert-${type} alert-dismissible" role="alert">`,
    `   <div>${message}</div>`,
    '   <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>',
    '</div>'
  ].join('');
  $("#errorInfo").html(wrapper);
}


const page = $("#page").val();
const size = $("#size").val();
const search_query = $("#search_query").val();
$("#search_input").val(search_query);
getAllPenerbit();
function getAllPenerbit(){
    const is_search = search_query === "" ? false : true;
    const url_query = is_search ? host+'/api/penerbit/result?search_query='+search_query+'&page='+page+'&size='+size : host+'/api/penerbit/map?page='+page+'&size='+size;
    $.ajax({
        url: url_query,
        type:'GET',
        contentType:'application/json',
        success:function(data){
            const halamanSekarang = data.halamanSekarang;
            const totalHalaman = data.totalHalaman;
            console.log(data);

            $("#penerbitData").html(``);
            const no_data = (halamanSekarang-1) * size;
            for(i = 0; i<data.dataApi.length; i++){
                $("#penerbitData").append(`
                    <tr>
                        <td>${i+1+no_data}</td>
                        <td>${data.dataApi[i].nama}</td>
                        <td>${data.dataApi[i].alamat}</td>
                        <td>${data.dataApi[i].telp}</td>
                        <th>
                            <button class="btn bg-warning" onclick="form_(${data.dataApi[i].id})"><i class="bi bi-pencil-square"></i>Edit</button>
                            <button class="btn bg-danger" onclick="delete_(${data.dataApi[i].id})"><i class="bi bi-trash3"></i>Delete</button>
                        </th>
                    </tr>
                `)
            }

            const url_page = is_search ? host+"/penerbit/result?search_query="+search_query+"&" : host+"/penerbit?";
            $("#halamanNav").html(``);
            if(halamanSekarang != 1){
                $("#halamanNav").append(
                `
                    <li class="page-item"><a class="page-link" href="${url_page}page=${halamanSekarang-1}&size=${size}">Previous</a></li>
                `
                );
            }
            for(i = 1; i <= data.totalHalaman; i++){
                if(i == data.halamanSekarang){
                    $("#halamanNav").append(`
                        <li class="page-item disabled"><a class="page-link" href="${url_page}page=${i}&size=${size}">${i}</a></li>
                    `);
                }else{
                    $("#halamanNav").append(`
                        <li class="page-item"><a class="page-link" href="${url_page}page=${i}&size=${size}">${i}</a></li>
                    `);
                }
            }
            if(halamanSekarang != totalHalaman){
                $("#halamanNav").append(
                `
                    <li class="page-item"><a class="page-link" href="${url_page}page=${halamanSekarang+1}&size=${size}">Next</a></li>
                `
                );
            }
        }
    })
}

$("#search_button").click(function(){
    const query = ($("#search_input").val()).trim();
    if(query != ""){
        window.location = host+'/penerbit/result?search_query='+query;
    }
})

function form_(id){
    var str = ``;
    str += `<div id="errorInfo" style="display: none;" ></div>`;
    if(id){
        $.ajax({
            url: host + '/api/penerbit/' + id,
            type: "get",
            contentType:'application/json',
            async:false,
            success:function(data){
                str += `Nama : <input class="form-control" type="text" id="nama" value="${data.nama}">`;
                str += `<small class="errorField text-danger" type="text" id="errNama"></small> <br>`;
                str += `Alamat : <input class="form-control" type="text" id="alamat" value="${data.alamat}">`;
                str += `<small class="errorField text-danger" type="text" id="errAlamat"></small> <br>`;
                str += `Telephone : <input class="form-control" type="text" id="telp" value="${data.telp}">`;
                str += `<small class="errorField text-danger" type="text" id="errTelp"></small> <br>`;
                str += `<hr>
                        <button class="btn btn-warning" onclick="editPenerbit(${data.id})">edit</button>
                        <button class="btn btn-primary" onclick="batal()">batal</button>`;
            }
        })
    }else{
        str += `Nama : <input class="form-control" type="text" id="nama">`;
        str += `<small class="errorField text-danger" type="text" id="errNama"></small> <br>`;
        str += `Alamat : <input class="form-control" type="text" id="alamat">`;
        str += `<small class="errorField text-danger" type="text" id="errAlamat"></small> <br>`;
        str += `Telephone : <input class="form-control" type="text" id="telp">`;
        str += `<small class="errorField text-danger" type="text" id="errTelp"></small> <br>`;
        str += `<hr>
                <button class="btn btn-success" onclick="simpanPenerbit()">simpan</button>
                <button class="btn btn-primary" onclick="batal()">batal</button>`;
    }

    $(".modal-body").html(str);
    $("#mymodal").modal("show");
    $(".modal-title").html("Penerbit Form");
}

function simpanPenerbit(){
    var nama = $("#nama").val();
    var alamat = $("#alamat").val();
    var telp = $("#telp").val();
    var verifNama = false;
    var verifAlamat = false;
    var verifTelp = false;
    $("#errNama").text("");
    $("#errAlamat").text("");
    $("#errTelp").text("");

    if(nama == ""){
       $("#errNama").text("*tidak boleh kosong");
       verifNama = true;
    }
    if(alamat == ""){
       $("#errAlamat").text("*tidak boleh kosong");
       verifAlamat = true;
    }
    if(telp == ""){
       $("#errTelp").text("*tidak boleh kosong");
       verifTelp = true;
    }
    if(verifNama || verifAlamat || verifTelp){
        return
    }

    const penerbit = {
        nama: nama,
        alamat: alamat,
        telp: telp,
    }

    $.ajax({
        url: host + '/api/penerbit',
        type: "POST",
        dataType: "JSON",
        data: JSON.stringify(penerbit),
        contentType: "application/json",
        success: function(result){
            if(result.status == "ada"){
                appendAlert('Error: data sudah ada', 'danger');
                $("#errorInfo").show();
            }else{
                $('#mymodal').modal('hide');
                getAllPenerbit();
            }
        }
    })
}
function editPenerbit(id){
    var nama = $("#nama").val();
    var alamat = $("#alamat").val();
    var telp = $("#telp").val();
    var verifNama = false;
    var verifAlamat = false;
    var verifTelp = false;
    $("#errNama").text("");
    $("#errAlamat").text("");
    $("#errTelp").text("");

    if(nama == ""){
       $("#errNama").text("*tidak boleh kosong");
       verifNama = true;
    }
    if(alamat == ""){
       $("#errAlamat").text("*tidak boleh kosong");
       verifAlamat = true;
    }
    if(telp == ""){
       $("#errTelp").text("*tidak boleh kosong");
       verifTelp = true;
    }
    if(verifNama || verifAlamat || verifTelp){
        return
    }

    const penerbit = {
        nama: nama,
        alamat: alamat,
        telp: telp,
    }

    $.ajax({
        url: host + '/api/penerbit/' + id,
        type: "PUT",
        dataType: "JSON",
        data: JSON.stringify(penerbit),
        contentType: "application/json",
        success: function(result){
            if(result.status == "ada"){
                appendAlert('Error: data sudah ada', 'danger');
                $("#errorInfo").show();
            }else{
                $('#mymodal').modal('hide');
                getAllPenerbit();
            }
        }
    })
}

function batal(){
    $("#mymodal").modal("hide");
}

function delete_(id){
    var str = ``;
    str = `<h5>Apakah anda yakin ingin menghapus data ini?</h5>`;
    str += `<hr><button class="btn btn-danger" onclick="deleteData(${id})">hapus</button>
                <button class="btn btn-primary" onclick="batal()">batal</button>`;
    $(".modal-body").html(str);
    $("#mymodal").modal("show");
    $(".modal-title").html("Delete penerbit")
}

function deleteData(id){
    $.ajax({
        url: host + '/api/penerbit/' + id,
        type:'DELETE',
        contentType:'application/json',
        success:function(result){
            alert("Berhasil");
            $("#mymodal").modal("hide");
            getAllPenerbit();
        }
    })
}

const appendAlert = (message, type) => {
  console.log("test");
  const wrapper = document.createElement('div');
  wrapper.innerHTML = [
    `<div class="alert alert-${type} alert-dismissible" role="alert">`,
    `   <div>${message}</div>`,
    '   <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>',
    '</div>'
  ].join('');
  $("#errorInfo").html(wrapper);
}

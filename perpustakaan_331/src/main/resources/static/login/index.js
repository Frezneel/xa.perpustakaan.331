function registerOnLogin(){
    $.ajax({
        url: "/register",
        type: "GET",
        contentType: "html",
        success: function(data){
            $(".modal-title").text("Daftar");
            $(".modal-body").html(data);
            $(".modal").modal("show");
        }
    });
}

function lupapassword(){
    $.ajax({
        url: "/lupapassword",
        type: "GET",
        contentType: "html",
        success: function(data){
            $(".modal-title").text("Lupa Password");
            $(".modal-body").html(data);
            $(".modal").modal("show");
        }
    });
}

$("#loginUserBtn").click(function(){
    buttonLoading(true);
    var email = $("#emailInput").val();
    var password = $("#passwordInput").val();
    var emailFormatValidation = /^[A-Za-z0-9_\-\.]+\@[A-Za-z0-9_\-\.]+\.[A-Za-z]{2,4}$/;
    var booleanEmail = false;
    var booleanPassword = false;
    $("#loginUserErr").text("");
    if(email === ""){
        $("#emailInputErr").text("*Masukkan email");
    }else if(!emailFormatValidation.test(email)){
        $("#emailInputErr").text("*Email tidak sesuai format!");
    }else{
        $("#emailInputErr").text("");
        booleanEmail = true;
    }
    if(password === ""){
        $("#passwordInputErr").text("*Masukkan password");
    }else{
        $("#passwordInputErr").text("");
        booleanPassword = true;
    }

    if(booleanEmail == false || booleanPassword == false){
        buttonLoading(false);
        return;
    }else if (booleanEmail == true && booleanPassword == true){
        const loginData = {
            email: email,
            password: password
        };

        $.ajax({
            url: api_v1 + "user/login",
            data: JSON.stringify(loginData),
            type: "POST",
            contentType: "application/json",
            success: function(data){
                if(data.info === "success"){
                    buttonLoading(false);
                    location.reload();
                }else if(data.info === "failed"){
                    $("#loginUserErr").text("*Email / password salah");
                    buttonLoading(false);
                    return;
                }else if(data.info === "locked"){
                    $("#loginUserErr").text("*Akun anda terkunci silakan hubungi admin");
                    buttonLoading(false);
                    return;
                }
            }
        });
    }
})

$("#eyeIconButton").click(function(){
    if($("#passwordInput").attr("type") == "password"){
        $("#passwordInput").attr("type", "text");
        $("#eyeIconLogin").removeClass("bi bi-eye-slash");
        $("#eyeIconLogin").addClass("bi bi-eye");
    }else if($("#passwordInput").attr("type") == "text"){
        $("#passwordInput").attr("type", "password");
        $("#eyeIconLogin").removeClass("bi bi-eye");
        $("#eyeIconLogin").addClass("bi bi-eye-slash");
    }
})

function buttonLoading(keterangan){
    if(keterangan === true){
        document.getElementById("loginUserBtn").disabled = true;
        document.getElementById("loginUserBtn").innerHTML = "<span class='spinner-border spinner-border-sm' role='status' aria-hidden='true'></span> Loading...";
    }else{
        document.getElementById("loginUserBtn").disabled = false;
        document.getElementById("loginUserBtn").innerHTML = "Masuk";
    }
}
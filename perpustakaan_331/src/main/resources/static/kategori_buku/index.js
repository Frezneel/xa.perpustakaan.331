getAllKategoriBuku();
function getAllKategoriBuku(){
    $.ajax({
        url: host + '/api/v2/kategori_buku',
        type:'GET',
        contentType:'application/json',
        success:function(data){
            $("#kategori_bukuData").html(``);
            for(i = 0; i<data.length; i++){
                $("#kategori_bukuData").append(`
                    <tr>
                        <td>${i+1}</td>
                        <td>${data[i].buku.judul}</td>
                        <td>${data[i].kategori.nama}</td>
                        <th>
                            <button class="btn bg-warning" onclick="form_(${data[i].id})"><i class="bi bi-pencil-square"></i>Edit</button>
                            <button class="btn bg-danger" onclick="delete_(${data[i].id})"><i class="bi bi-trash3"></i>Delete</button>
                        </th>
                    </tr>
                `)
            }
        }
    })
}

function form_(id){
    var str = ``;
    var buku;
    var kategori;
    $.ajax({
        url: host + '/api/buku',
        type:'GET',
        contentType:'application/json',
        async:false,
        success:function(data){
            buku = data;
        }
    })
    $.ajax({
        url: host + '/api/kategori',
        type:'GET',
        contentType:'application/json',
        async:false,
        success:function(data){
            kategori = data;
        }
    })
    str += `<div id="errorInfo" style="display: none;">
            </div>`;
    if(id){
        $.ajax({
            url: host + '/api/v2/kategori_buku/' + id,
            type: "get",
            contentType:'application/json',
            async:false,
            success:function(data){
                str += `Buku : <select class="form-select" id="buku">`;
                for(i=0; i < buku.length; i++){
                   if(data.buku_id === buku[i].id){
                      str += `<option value="${buku[i].id}" selected>${buku[i].judul}</option>`;
                   }else{
                      str += `<option value="${buku[i].id}">${buku[i].judul}</option>`;
                   }
                }
                str += `</select>`;
                str += `<small class="errorField text-danger" type="text" id="errBuku"></small> <br>`;
                str += `Kategori : <select class="form-select" id="kategori">`;
                for(i=0; i < kategori.length; i++){
                    if(data.kategori_id === kategori[i].id){
                        str += `<option value="${kategori[i].id}" selected>${kategori[i].nama}</option>`;
                    }else{
                        str += `<option value="${kategori[i].id}">${kategori[i].nama}</option>`;
                    }
                }
                str += `</select>`;
                str += `<small class="errorField text-danger" type="text" id="errKategori"></small> <br>`;
                str += `<hr>
                        <button class="btn btn-warning" onclick="editKategoriBuku(${data.id})">edit</button>
                        <button class="btn btn-primary" onclick="batal()">batal</button>`;
            }
        })
    }else{
        str += `Buku : <select class="form-select" id="buku">`;
        for(i=0; i < buku.length; i++){
          str += `<option value="${buku[i].id}">${buku[i].judul}</option>`;
        }
        str += `</select>`;
        str += `<small class="errorField text-danger" type="text" id="errBuku"></small> <br>`;
        str += `Kategori : <select class="form-select" id="kategori">`;
        for(i=0; i < kategori.length; i++){
          str += `<option value="${kategori[i].id}">${kategori[i].nama}</option>`;
        }
        str += `</select>`;
        str += `<small class="errorField text-danger" type="text" id="errKategori"></small> <br>`;

        str += `<hr>
                <button class="btn btn-success" onclick="simpanKategoriBuku()">simpan</button>
                <button class="btn btn-primary" onclick="batal()">batal</button>`;
    }

    $(".modal-body").html(str);
    $("#mymodal").modal("show");
    $(".modal-title").html("Kategori Buku Form")
}

function batal(){
    $("#mymodal").modal("hide");
}

function simpanKategoriBuku(){
    var buku = $("#buku").val();
    var kategori = $("#kategori").val();
    var verifBuku = false;
    var verifKategori = false;
    $("#errBuku").text("");
    $("#errKategori").text("");

    if(buku == ""){
       $("#errBuku").text("*tidak boleh kosong");
       verifBuku = true;
    }
    if(kategori == ""){
       $("#errKategori").text("*tidak boleh kosong");
       verifKategori = true;
    }

    if(verifBuku || verifKategori){
        return
    }

    const kategori_buku = {
        buku_id: buku,
        kategori_id: kategori
    }

    $.ajax({
        url: host + '/api/v2/kategori_buku',
        type: "POST",
        dataType: "JSON",
        data: JSON.stringify(kategori_buku),
        contentType: "application/json",
        success: function(result){
            if(result.status == "ada"){
                appendAlert('Error: kategori pada judul sudah ada', 'danger');
                $("#errorInfo").show();
            }else{
                $('#mymodal').modal('hide');
                getAllKategoriBuku();
            }
        }
    })
}

function editKategoriBuku(id){
    var buku = $("#buku").val();
    var kategori = $("#kategori").val();
    var verifBuku = false;
    var verifKategori = false;
    $("#errBuku").text("");
    $("#errKategori").text("");

    if(buku == ""){
       $("#errBuku").text("*tidak boleh kosong");
       verifBuku = true;
    }
    if(kategori == ""){
       $("#errKategori").text("*tidak boleh kosong");
       verifKategori = true;
    }

    if(verifBuku || verifKategori){
        return
    }

    const kategori_buku = {
        buku_id: buku,
        kategori_id: kategori
    }

    $.ajax({
        url: host + '/api/v2/kategori_buku/' + id,
        type: "PUT",
        dataType: "JSON",
        data: JSON.stringify(kategori_buku),
        contentType: "application/json",
        success: function(result){
            if(result.status == "ada"){
                appendAlert('Error: kategori pada judul sudah ada', 'danger');
                $("#errorInfo").show();
            }else{
                $('#mymodal').modal('hide');
                getAllKategoriBuku();
            }
        }
    })
}

function delete_(id){
    var str = ``;
    str = `<h5>Apakah anda yakin ingin menghapus data ini?</h5>`;
    str += `<hr><button class="btn btn-danger" onclick="deleteData(${id})">hapus</button>
                <button class="btn btn-primary" onclick="batal()">batal</button>`;
    $(".modal-body").html(str);
    $("#mymodal").modal("show");
    $(".modal-title").html("Delete kategori buku")
}

function deleteData(id){
    $.ajax({
        url: host + '/api/v2/kategori_buku/' + id,
        type:'DELETE',
        contentType:'application/json',
        success:function(result){
            alert("Berhasil");
            $("#mymodal").modal("hide");
            getAllKategoriBuku();
        }
    })
}

const appendAlert = (message, type) => {
  console.log("test");
  const wrapper = document.createElement('div');
  wrapper.innerHTML = [
    `<div class="alert alert-${type} alert-dismissible" role="alert">`,
    `   <div>${message}</div>`,
    '   <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>',
    '</div>'
  ].join('');
  $("#errorInfo").html(wrapper);
}

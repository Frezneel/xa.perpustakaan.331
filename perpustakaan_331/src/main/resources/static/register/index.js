$(document).ready(function(){
    const data_validation = {};
    document.getElementById("emailDaftar").onchange = function() {
        const inputCek_email = "span_email";
        inputCek("loading", inputCek_email);
        const emailInput = $("#emailDaftar").val();
        const emailFormatValidation = /^[A-Za-z0-9_\-\.]+\@[A-Za-z0-9_\-\.]+\.[A-Za-z]{2,4}$/;
        if(!emailFormatValidation.test(emailInput)){
            $("#emailDaftarErr").text("*Email tidak sesuai format!");
            inputCek("invalid", inputCek_email);
            return
        }
        else{
            $("#emailDaftarErr").text("");
        }
        $.ajax({
            url: api_v1 + "user/mail=" + emailInput,
            type: "GET",
            contentType: "html",
            success: function(data){
               if(data.status === "found"){
                   document.getElementById("emailDaftarErr").className = "text-danger";
                   $("#emailDaftarErr").text("*Email telah terdaftar!");
                   inputCek("invalid", inputCek_email);
                   data_validation.email_is = true;
                   return;
               }else if (data.status === "empty"){
                   document.getElementById("emailDaftarErr").className = "text-success";
                   inputCek("valid", inputCek_email);
                   $("#emailDaftarErr").text("*Email belum terdaftar, dapat digunakan");
                   tokenToExpired(emailInput, false);
                   data_validation.email_is = true;
                   loadInputOTP(data_validation);
                   is_done(data_validation);
               }
            }
        });
    }

    function loadInputOTP(data_validation){
        const email = $("#emailDaftar").val();
        var isOtpValid = false;
        if($("#otpForm").html() === ""){
            const htmlOTP = `
                <div>Verifikasi OTP*</div>
                <div class="input-group">
                    <button class="btn bg-success text-light " id="button_otp">Kirim OTP</button>
                    <input id="otpDaftar" class="form-control" maxlength="6">
                    <span class="input-group-text bg-secondary" id="span_otp"></span>
                </div>
                <small id="otpDaftarErr" class="text-danger"></small>
            `;
            $("#otpForm").html(htmlOTP);
            $("#button_otp").click(function(){
                setCountDown();
                tokenToExpired(email, true);
            })
            document.getElementById("otpDaftar").onchange = function() {
                const tokenInput = $("#otpDaftar").val();
                const span_otp = "span_otp";
                if(tokenInput.length == 6){
                    inputCek("loading", span_otp);
                    $.ajax({
                        url: api_v1 + "token/register/result?email=" + email + "&used_for=Pendaftaran&token=" + tokenInput,
                        type: "GET",
                        contentType: "application/json",
                        success: function(data){
                           if(data.info === "valid"){
                               const isOtpValid = true;
                               data_validation.otp_is = true;
                               inputCek("valid", span_otp);
                               $("#otpDaftarErr").text("");
                               clearInterval(timeDown);
                               document.getElementById("button_otp").remove();
                               document.getElementById("emailDaftar").disabled = true;
                               loadPasswordInput(data_validation);
                               loadInputBiodata(data_validation);
                               is_done(data_validation)
                           }else if(data.info === "expired"){
                               inputCek("invalid", span_otp);
                               data_validation.otp_is = false;
                               $("#otpDaftarErr").text("*Kode OTP kadaluarsa, silakan kirim ulang OTP");
                               is_done(data_validation)
                               return;
                           }else if(data.info === "invalid"){
                               inputCek("invalid", span_otp);
                               data_validation.otp_is = false;
                               $("#otpDaftarErr").text("*Kode OTP salah");
                               is_done(data_validation)
                               return;
                           }
                        }
                    });
                }
            }
        }
    }

    function loadPasswordInput(data_validation){
        document.getElementById("otpDaftar").disabled = true;
        if($("#passwordForm").html() === ""){
            const email = $("#emailDaftar").val();
            const htmlPassword = `
                <label for="passwordInput">Password*</label>
                <div class="input-group mb-1">
                    <button class="btn bg-secondary" id="eyeIconRegisterButton">
                        <i id="eyeIconRegister" class="bi bi-eye-slash" aria-hidden="true"></i>
                    </button>
                    <input id="passwordInput" type="password" class="form-control">
                    <span class="input-group-text bg-secondary" id="span_password"></span>
                </div>
                <div class="container-fluid row" id="strong_password">
                    <div class="progress p-0 me-1" style="width: 20%; height: 10px;">
                        <div id="pgb1" class="progress-bar bg-white" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
                    </div>
                    <div class="progress p-0 me-1" style="width: 20%; height: 10px;">
                        <div id="pgb2" class="progress-bar bg-white" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
                    </div>
                    <div class="progress p-0 me-1" style="width: 20%; height: 10px;">
                        <div id="pgb3" class="progress-bar bg-white" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
                    </div>
                 </div>
                 <small id="passwordInputErr" class="text-danger"></small>
            `;
            const htmlPasswordConfirm = `
                <label for="passwordConfirmInput">Password konfirmasi*</label>
                <div class="input-group mb-1">
                    <button class="btn bg-secondary" id="eyeIconConfirmRegisterButton">
                        <i id="eyeIconConfirmRegister" class="bi bi-eye-slash" aria-hidden="true"></i>
                    </button>
                    <input id="passwordConfirmInput" type="password" class="form-control">
                    <span class="input-group-text bg-secondary" id="span_passwordConfirm"></span>
                </div>
                <small id="passwordConfirmInputErr" class="text-danger"></small>
            `;
            $("#passwordForm").html(htmlPassword);
            $("#passwordConfirmForm").html(htmlPasswordConfirm);
            const span_password = "span_password";

            document.getElementById("passwordInput").oninput = function(){
                const passwordInput = $("#passwordInput").val();
                const passwordConfirmInput = $("#passwordConfirmInput").val();
                const strongLevel = strongPasswordCheck(passwordInput);
                console.log(strongLevel);
                inputCek("loading", span_password);
                const pgb1 = document.getElementById("pgb1");
                const pgb2 = document.getElementById("pgb2");
                const pgb3 = document.getElementById("pgb3");
                if(strongLevel < 2){
                   pgb1.className = "progress-bar bg-danger" ;
                   pgb2.className = "progress-bar bg-white";
                   pgb3.className = "progress-bar bg-white";
                   data_validation.password_is = false;
                   inputCek("invalid", span_password);
                   is_done(data_validation)
                }else if(strongLevel < 2.5){
                   pgb1.className = "progress-bar bg-warning";
                   pgb2.className = "progress-bar bg-warning";
                   pgb3.className = "progress-bar bg-white";
                   data_validation.password_is = true;
                   inputCek("valid", span_password);
                   is_done(data_validation)
                }else if(strongLevel >= 2.5){
                   pgb1.className ="progress-bar bg-success";
                   pgb2.className = "progress-bar bg-success";
                   pgb3.className = "progress-bar bg-success";
                   data_validation.password_is = true;
                   inputCek("valid", span_password);
                   is_done(data_validation)
                }
                if(passwordConfirmInput != ""){
                    validationPassword(passwordInput, passwordConfirmInput);
                }
            }

            document.getElementById("passwordConfirmInput").oninput = function() {
                const passwordInput = $("#passwordInput").val();
                const passwordConfirmInput = $("#passwordConfirmInput").val();
                validationPassword(passwordInput, passwordConfirmInput);
            }

            function validationPassword(passwordInput, passwordConfirmInput){
                const span_passwordConfirm = "span_passwordConfirm";
                inputCek("loading", span_passwordConfirm);
                if(passwordInput === passwordConfirmInput){
                    data_validation.password_confirm_is = true;
                    inputCek("valid", span_passwordConfirm);
                    loadInputBiodata(data_validation);
                    is_done(data_validation)
                    $("#passwordConfirmInputErr").text("");
                }else{
                    data_validation.password_confirm_is = false;
                    is_done(data_validation)
                    inputCek("invalid", span_passwordConfirm);
                    $("#passwordConfirmInputErr").text("*Password tidak sama");
                }
            }

            // Mode Hidden atau Show
            $("#eyeIconRegisterButton").click(function(){
                if($("#passwordInput").attr("type") == "password"){
                    $("#passwordInput").attr("type", "text");
                    $("#eyeIconRegister").removeClass("bi bi-eye-slash");
                    $("#eyeIconRegister").addClass("bi bi-eye");
                }else if($("#passwordInput").attr("type") == "text"){
                    $("#passwordInput").attr("type", "password");
                    $("#eyeIconRegister").removeClass("bi bi-eye");
                    $("#eyeIconRegister").addClass("bi bi-eye-slash");
                }
            })
            $("#eyeIconConfirmRegisterButton").click(function(){
                if($("#passwordConfirmInput").attr("type") == "password"){
                    $("#passwordConfirmInput").attr("type", "text");
                    $("#eyeIconConfirmRegister").removeClass("bi bi-eye-slash");
                    $("#eyeIconConfirmRegister").addClass("bi bi-eye");
                }else if($("#passwordConfirmInput").attr("type") == "text"){
                    $("#passwordConfirmInput").attr("type", "password");
                    $("#eyeIconConfirmRegister").removeClass("bi bi-eye");
                    $("#eyeIconConfirmRegister").addClass("bi bi-eye-slash");
                }
            })
        }
    }

    function loadInputBiodata(data_validation){
        const is_next = data_validation.password_is && data_validation.password_confirm_is;
        if(is_next && $("#biodataForm").html() === ""){
            const htmlBiodata = `
                <div class="mb-2">
                    <label for="fullnameInput">Nama lengkap*</label>
                    <div class="input-group mb-1">
                        <input id="fullnameInput" class="form-control" type="text">
                        <span class="input-group-text bg-secondary" id="span_fullnameConfirm"></span>
                    </div>
                    <small id="fullnameInputErr" class="text-danger"></small>
                </div>
                <div class="mb-2">
                    <label for="phoneNumber">Nomor handphone</label>
                    <div class="input-group">
                        <span class="input-group-text bg-secondary">+62 </span>
                        <input id="phoneNumber" type="number" i="" class="form-control" placeholder="8123456789" onkeypress="return (event.charCode !=8 &amp;&amp; event.charCode ==0 || (event.charCode >= 48 &amp;&amp; event.charCode <= 57))">
                        <span class="input-group-text bg-secondary" id="span_phoneConfirm"></span>
                    </div>
                    <small id="phoneNumberErr" class="text-danger"></small>
                </div>
                <div class="mb-2">
                    <label for="roleSelect">Daftar sebagai*</label>
                    <div class="input-group">
                        <select id="roleSelect" class="form-select">
                            <option value="" selected="selected">Pilih</option>
                        </select>
                    <span class="input-group-text bg-secondary" id="span_roleConfirm"></span>
                    </div>
                    <small id="roleSelectErr" class="text-danger"></small>
                </div>
            `;
            $("#biodataForm").html(htmlBiodata);

            $.ajax({
                url: api_v1 + "role",
                type: "GET",
                contentType: "application/json",
                success:function(data){
                    const roleSelect = document.getElementById("roleSelect");
                    for(i = 0; i<data.length; i++){
                        roleSelect.options[roleSelect.options.length] = new Option(data[i].name, data[i].id);
                    }
                }
            });

            document.getElementById("fullnameInput").oninput = function(){
                const nameInput = $("#fullnameInput").val();
                const span_fullnameConfirm = "span_fullnameConfirm";
                inputCek("loading", span_fullnameConfirm);
                if(nameInput === ""){
                    $("#fullnameInputErr").text("*tidak boleh kosong");
                    inputCek("invalid", span_fullnameConfirm);
                    data_validation.fullname_is = false;
                    is_done(data_validation)
                }else if(nameInput.trim() != nameInput){
                    $("#fullnameInputErr").text("*terdapat whitespace");
                    inputCek("invalid", span_fullnameConfirm);
                    data_validation.fullname_is = false;
                    is_done(data_validation)
                }else{
                    $("#fullnameInputErr").text("");
                    inputCek("valid", span_fullnameConfirm);
                    data_validation.fullname_is = true;
                    is_done(data_validation)
                }
            }

            data_validation.phone_is = true;
            inputCek("valid", span_phoneConfirm);
            document.getElementById("phoneNumber").oninput = function(){
                const phoneInput = $("#phoneNumber").val();
                const span_phoneConfirm = "span_phoneConfirm";
                inputCek("loading", span_phoneConfirm);
                const numberFormat = /[0-9]/;
                if(phoneInput === ""){
                    $("#phoneNumberErr").text("");
                    inputCek("valid", span_phoneConfirm);
                    data_validation.phone_is = true;
                    is_done(data_validation)
                }else if(numberFormat.test(phoneNumber) && phoneInput != ""){
                    inputCek("invalid", span_phoneConfirm);
                    $("#phoneNumberErr").text("*Hanya boleh angka saja");
                    data_validation.phone_is = false;
                    is_done(data_validation)
                }else {
                    $("#phoneNumberErr").text("");
                    inputCek("valid", span_phoneConfirm);
                    data_validation.phone_is = true;
                    is_done(data_validation)
                }
            }
            document.getElementById("roleSelect").onchange = function(){
                const roleSelect = $("#roleSelect").val();
                const span_roleConfirm = "span_roleConfirm";
                inputCek("loading", span_roleConfirm);

                if(roleSelect === ""){
                    inputCek("invalid", span_roleConfirm);
                    $("#roleSelectErr").text("*Pilih role yang tersedia");
                    data_validation.role_is = false;
                    is_done(data_validation)
                }else{
                    $("#roleSelectErr").text("");
                    inputCek("valid", span_roleConfirm);
                    data_validation.role_is = true;
                    is_done(data_validation)
                }
            }
        }
    }

    function is_done(data_validation){
        try{
            const is_email = data_validation.email_is;
            const is_otp = data_validation.otp_is;
            const is_password = data_validation.password_is;
            const is_password_confirm = data_validation.password_confirm_is;
            const is_fullname = data_validation.fullname_is;
            const is_phone = data_validation.phone_is;
            const is_role = data_validation.role_is;
            const done = is_email && is_otp && is_password && is_password_confirm && is_fullname && is_phone && is_role;
            console.log(data_validation);
            if(done){
                document.getElementById("buttonDaftar").disabled = false;
                $("#buttonDaftar").click(function(){
                    const email = $("#emailDaftar").val();
                    const password = $("#passwordInput").val();
                    const fullname = $("#fullnameInput").val();
                    const phone = $("#phoneNumber").val();
                    const role = $("#roleSelect").val();

                    const biodata = {
                        fullname: fullname,
                        mobile_phone: phone
                    }
                    $.ajax({
                        url: api_v1 + "biodata",
                        type: "POST",
                        data : JSON.stringify(biodata),
                        contentType: "application/json",
                        success:function(REST){
                            const user = {
                                email: email,
                                password: password,
                                role_id: role,
                                biodata_id: REST.data.id
                            }
                            $.ajax({
                                url: api_v1 + "user",
                                type: "POST",
                                data : JSON.stringify(user),
                                contentType: "application/json",
                                success:function(data){
                                    document.getElementById("buttonDaftar").style.visibility = "hidden";
                                    document.getElementById("daftarForm").remove();
                                    appendAlert("Info: Data berhasil didaftarakan","success");
                                    $("#errorInfo").show();
                                    $("#closemodal").click(function() {
                                        $('#modalwindow').modal('hide');
                                        location.reload();
                                    });
                                }
                            });
                        }
                    });
                })
            }else{
                document.getElementById("buttonDaftar").disabled = true;
            }
        }catch(err){
            document.getElementById("buttonDaftar").disabled = true;
            return;
        }
    }

    function strongPasswordCheck(password){
        var angka = /[0-9]/;
        var hurufkecil = /[a-z]/;
        var hurufbesar = /[A-Z]/;
        var special = /[~`!@#$%^&*\(\)\-_+=\{\}\[\]|\\\;:\<>,.\"/?]/;
        var whitespace = /\s/;
        var is_whitespace = false;
        var textValidasi = "";

        var point = 0;

        if(password === ""){
            textValidasi += "*Masukkan password: "
        }

        //Strong password
        if(password.length < 8){
            if(textValidasi === ""){
                textValidasi += "*"
            }
            textValidasi += "Panjang minimal 8, ";
        }else{point += 1}
        if(!angka.test(password)){
            if(textValidasi === ""){
                textValidasi += "*Harus "
            }
            textValidasi += "terdapat angka, ";
        }else{point += 0.4}
        if(!hurufkecil.test(password)){
            if(textValidasi === ""){
                textValidasi += "*Harus "
            }
            textValidasi += "terdapat huruf kecil, ";
        }else{point += 0.4}
        if(!hurufbesar.test(password)){
            if(textValidasi === ""){
                textValidasi += "*Harus "
            }
            textValidasi += "terdapat huruf besar, ";
        }else{point += 0.4}
        if(!special.test(password)){
            if(textValidasi === ""){
                textValidasi += "*Tambahkan "
            }
            textValidasi += "spesial karakter akan lebih aman, ";
        }else{point += 0.6}
        if(whitespace.test(password)){
            if(textValidasi === ""){
                textValidasi += "*"
            }
            textValidasi += "tidak boleh ada whitespace";
            whitespace = true;
        }
        if(textValidasi != "" && (point < 2 || is_whitespace)){
            $("#passwordInputErr").text(textValidasi);
            return point;
        }else if((textValidasi != "" ||textValidasi == "") && point >= 2 && !is_whitespace){
            $("#passwordInputErr").text(textValidasi);
            boleanPassword = true;
            return point;
        }
    }

    function tokenToExpired(email, is_send){
        $.ajax({
            url: api_v1 + "token/tokenToExpired?email="+email+"&used_for=Pendaftaran",
            type: "PUT",
            contentType: "application/json",
            success: function(data){
               if(is_send){
                   sentOTP(email);
               }
            }
        });
    }

    function sentOTP(emailInput){
       $.ajax({
           url: api_v2 + "mail/otp/register?mailTo=" + emailInput,
           type: "POST",
           contentType: "application/json",
           success: function(data){
              if(data.status === "success"){
                  postToken(emailInput, data.otp);
              }else{
                  alert("Server gagal mengirimkan OTP");
                  buttonLoading(false);
                  return;
              }
           },
           error: function(){
               buttonLoading(false);
               return;
           }
       });
    }

    function postToken(email, token){
        const data_token = {
            email: email,
            token: token,
            is_expired: false,
            used_for: "Pendaftaran"
        };
        const myJson = JSON.stringify(data_token);
        $.ajax({
            url : api_v1 + "token",
            type : "POST",
            contentType : "application/json",
            data : myJson,
            success : function(data){
            },
            error: function(){
                alert("Terjadi kesalahan")
                buttonLoading(false);
                return;
            }
        });
    }

    function buttonLoading(keterangan){
        if(keterangan === true){
            document.getElementById("btnKirimOtp").disabled = true;
            document.getElementById("emailDaftar").disabled = true;
            document.getElementById("btnKirimOtp").innerHTML = "<span class='spinner-border spinner-border-sm' role='status' aria-hidden='true'></span> Loading...";
        }else{
            document.getElementById("btnKirimOtp").disabled = false;
            document.getElementById("emailDaftar").disabled = false;
            document.getElementById("btnKirimOtp").innerHTML = "Kirim OTP";
        }
    }

    function inputCek(info, id){
        if(info === "loading"){
            document.getElementById(id).innerHTML = `<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true" style="color: #43E5EB;"></span>`;
        }else if(info === "invalid"){
            document.getElementById(id).innerHTML = `<i class="bi bi-x-circle" style="font-size: 1rem; color: #FC222D;"></i>`;
        }else if(info === "valid"){
            document.getElementById(id).innerHTML = `<i class="bi bi-check-circle" style="font-size: 1rem; color: #5BFF5B;"></i>`;
        }
    }

    function setCountDown(){
        document.getElementById("button_otp").disabled = true;
        countDownDate = new Date().getTime() + 32000;
        timeDown = setInterval(countDownHtml, 1000);
    }

    function countDownHtml(){
        // ambil waktu dalam milisecond
        const now = new Date().getTime();
        // ambil jarak waktu 3 menit dengan cara deklarasi waktu diatas 3 menit dari sekarang
        const distance = countDownDate - now;
        // ambil nilai menit and detik
        var menit = String(Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60)) );
        var detik = String(Math.floor((distance % (1000 * 60)) / 1000) );
        if (menit.length != 2){
            menit = "0" + menit;
        }
        if (detik.length != 2){
            detik = "0" + detik;
        }
        document.getElementById("button_otp").innerHTML = menit + " : " + detik;
        if (distance < 999) {
            clearInterval(timeDown);
            document.getElementById("button_otp").disabled = false;
            document.getElementById("button_otp").innerHTML = "Kirim OTP";
        }
    }

    appendAlert = (message, type) => {
      const wrapper = document.createElement('div');
      wrapper.innerHTML = [
        `<div class="alert alert-${type} alert-dismissible" role="alert">`,
        `   <div>${message}</div>`,
        '</div>'
      ].join('');
      $("#errorInfo").html(wrapper);
    }

    $("#closemodal").click(function() {
        if(timeDown){
            clearInterval(timeDown);
        }
        $('#modalwindow').modal('hide');
        $(".modal-body").html(``);
    });
})
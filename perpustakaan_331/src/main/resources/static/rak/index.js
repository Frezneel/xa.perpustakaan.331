getAllRak();
function getAllRak(){
    $.ajax({
        url: host + '/api/rak',
        type:'GET',
        contentType:'application/json',
        success:function(data){
            $("#rakData").html(``);
            for(i = 0; i<data.length; i++){
                $("#rakData").append(`
                    <tr>
                        <td>${i+1}</td>
                        <td>${data[i].lokasi}</td>
                        <th>
                            <button class="btn bg-warning" onclick="form_(${data[i].id})"><i class="bi bi-pencil-square"></i>Edit</button>
                            <button class="btn bg-danger" onclick="delete_(${data[i].id})"><i class="bi bi-trash3"></i>Delete</button>
                        </th>
                    </tr>
                `)
            }
        }
    })
}

function form_(id){
    var str = ``;
    str += `<div id="errorInfo" style="display: none;">
            </div>`;
    if(id){
        $.ajax({
            url: host + '/api/rak/' + id,
            type: "get",
            contentType:'application/json',
            async:false,
            success:function(data){
                str += `Lokasi : <input class="form-control" type="text" id="lokasi" value="${data.lokasi}">`;
                str += `<small class="errorField text-danger" type="text" id="errLokasi"></small> <br>`;
                str += `<hr>
                        <button class="btn btn-warning" onclick="editRak(${data.id})">edit</button>
                        <button class="btn btn-primary" onclick="batal()">batal</button>`;
            }
        })
    }else{
        str += `Lokasi : <input class="form-control" type="text" id="lokasi">`;
        str += `<small class="errorField text-danger" type="text" id="errLokasi"></small> <br>`;
        str += `<hr>
                <button class="btn btn-success" onclick="simpanRak()">simpan</button>
                <button class="btn btn-primary" onclick="batal()">batal</button>`;
    }

    $(".modal-body").html(str);
    $("#mymodal").modal("show");
    $(".modal-title").html("Rak Form")
}

function batal(){
    $("#mymodal").modal("hide");
}

function simpanRak(){
    var lokasi = $("#lokasi").val();
    var verifLokasi = false;
    $("#errLokasi").text("");

    if(lokasi == ""){
       $("#errLokasi").text("*tidak boleh kosong");
       verifLokasi = true;
    }
    if(verifLokasi){
        return
    }

    const rak = {
        lokasi: lokasi
    }

    $.ajax({
        url: host + '/api/rak',
        type: "POST",
        dataType: "JSON",
        data: JSON.stringify(rak),
        contentType: "application/json",
        success: function(result){
            if(result.status == "ada"){
                appendAlert('Error: data sudah ada', 'danger');
                $("#errorInfo").show();
            }else{
                $('#mymodal').modal('hide');
                getAllRak();
            }
        }
    })
}

function editRak(id){
    var lokasi = $("#lokasi").val();
    var verifLokasi = false;
    $("#errLokasi").text("");

    if(lokasi == ""){
       $("#errLokasi").text("*tidak boleh kosong");
       verifLokasi = true;
    }
    if(verifLokasi){
        return
    }

    const rak = {
        lokasi: lokasi
    }

    $.ajax({
        url: host + '/api/rak/' + id,
        type: "PUT",
        dataType: "JSON",
        data: JSON.stringify(rak),
        contentType: "application/json",
        success: function(result){
            if(result.status == "ada"){
                appendAlert('Error: data sudah ada', 'danger');
                $("#errorInfo").show();
            }else{
                $('#mymodal').modal('hide');
                getAllRak();
            }
        }
    })
}

function delete_(id){
    var str = ``;
    str = `<h5>Apakah anda yakin ingin menghapus data ini?</h5>`;
    str += `<hr><button class="btn btn-danger" onclick="deleteData(${id})">hapus</button>
                <button class="btn btn-primary" onclick="batal()">batal</button>`;
    $(".modal-body").html(str);
    $("#mymodal").modal("show");
    $(".modal-title").html("Delete rak")
}

function deleteData(id){
    $.ajax({
        url: host + '/api/rak/' + id,
        type:'DELETE',
        contentType:'application/json',
        success:function(result){
            alert("Berhasil");
            $("#mymodal").modal("hide");
            getAllRak();
        }
    })
}

const appendAlert = (message, type) => {
  console.log("test");
  const wrapper = document.createElement('div');
  wrapper.innerHTML = [
    `<div class="alert alert-${type} alert-dismissible" role="alert">`,
    `   <div>${message}</div>`,
    '   <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>',
    '</div>'
  ].join('');
  $("#errorInfo").html(wrapper);
}

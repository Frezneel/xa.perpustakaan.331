getSession();

function getSession(){
    $.ajax({
        url: api_v1 + "user/session",
        type: "GET",
        contentType: "application/json",
        success: function(data){
            if(data.status === "empty"){
                $("#containerUser").html(
                `<div class="col-auto">
                     <button id="loginButton" class="btn btn-info content-end" type="button">
                         <i class="bi bi-person-circle"></i>
                         Login
                     </button>
                 </div>
                `
                );

                $("#loginButton").click(function(){
                    $.ajax({
                        url: "/login",
                        type: "GET",
                        contentType: "html",
                        success: function(data){
                            $(".modal-title").text("Masuk");
                            $(".modal-body").html(data);
                            $(".modal").modal({backdrop: 'static', keyboard: false});
                            $(".modal").modal("show");
                        }
                    });
                })
            }else{
                getUser(data.user_id);
            }
        }
    })
}

function getUser(id){
    $.ajax({
        url: api_v1 + "user/"+id,
        type: "GET",
        contentType: "application/json",
        success: function(data){
            $("#containerUser").html(
                `
                <div class="col-auto me-2">
                    <button id="userInfo" class="btn btn-light content-end" type="button">
                            Halo, ${data.biodata.fullname}
                    </button>
                </div>
                <div class="col-auto">
                    <button id="logoutButton" class="btn btn-danger content-end" type="button">
                            Keluar
                    </button>
                </div>
                `
            );
            $("#logoutButton").click(function(){
                $.ajax({
                    url: "/api/user/logout",
                    type: "POST",
                    contentType: "application/json",
                    success: function(data){
                        location.reload();
                    }
                });
            })
        }
    });
}

const search = document.getElementById("search_input");
search.addEventListener("keypress", function(event) {
  if (event.key === "Enter") {
    document.getElementById("search_button").click();
  }
});

$("#closemodal").click(function() {
    $('#modalwindow').modal('hide');
    $(".modal-body").html(``);
});
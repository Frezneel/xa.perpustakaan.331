package xa.batch331.perpustakaan_331.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import xa.batch331.perpustakaan_331.models.Pengarang;
import xa.batch331.perpustakaan_331.repositories.PengarangRepo;

import java.util.List;
import java.util.Optional;

@Service
public class PengarangService {
    @Autowired
    private PengarangRepo pengarangRepo;

    //GET
    public List<Pengarang> getAllPengarang(){
        return this.pengarangRepo.getAllPengarang();
    }

    public Optional<Pengarang> searchByNama(String nama){
        return this.pengarangRepo.searchByNama(nama);
    }

    public Optional<Pengarang> getPengarangById(Long id){
        return this.pengarangRepo.getPengarangById(id);
    }
    //pagination
    public Page<Pengarang> getAllPengarang(Pageable pageable){
        return this.pengarangRepo.getAllPengarang(pageable);
    }

    public Page<Pengarang> searchQueryPengarang(Pageable pageable, String search_query){
        return this.pengarangRepo.searchQueryPengarang(pageable, search_query);
    }
    //POST || PUT
    public void savePengarang(Pengarang pengarang){
        this.pengarangRepo.save(pengarang);
    }

    //DELETE
    public void deletePengarang(Long id){
        this.pengarangRepo.deleteById(id);
    }
}

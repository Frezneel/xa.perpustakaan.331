package xa.batch331.perpustakaan_331.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xa.batch331.perpustakaan_331.models.KategoriBuku;
import xa.batch331.perpustakaan_331.repositories.KategoriBukuRepo;

import java.util.List;
import java.util.Optional;

@Service
public class KategoriBukuService {

    @Autowired
    private KategoriBukuRepo kategoriBukuRepo;

    public List<KategoriBuku> getAllKategoriBuku(){
        return this.kategoriBukuRepo.getAllBuku();
    }

    public Optional<KategoriBuku> getKategoriBukuById(Long id){
        return this.kategoriBukuRepo.getBukuById(id);
    }

    public void saveKategoriBuku(KategoriBuku KategoriBuku){
        this.kategoriBukuRepo.save(KategoriBuku);
    }

    public void deleteKategoriBuku(Long id){
        this.kategoriBukuRepo.deleteById(id);
    }

    public Optional<KategoriBuku> saveValidation(Long buku_id, Long kategori_id){
        return this.kategoriBukuRepo.saveValidation(buku_id, kategori_id);
    }
}

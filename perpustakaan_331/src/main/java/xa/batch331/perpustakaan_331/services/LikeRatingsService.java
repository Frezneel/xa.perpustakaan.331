package xa.batch331.perpustakaan_331.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xa.batch331.perpustakaan_331.models.LikeRatings;
import xa.batch331.perpustakaan_331.repositories.LikeRatingsRepo;

import java.util.List;
import java.util.Optional;

@Service
public class LikeRatingsService {

    @Autowired
    private LikeRatingsRepo ratingsRepo;

    public List<LikeRatings> getAllRating(){
        return this.ratingsRepo.findAll();
    }

    public List<LikeRatings> getLikeRatingByBuku(Long id){
        return this.ratingsRepo.getLikeRatingByBuku(id);
    }

    public Optional<LikeRatings> getLikeRatingById(Long id){
        return this.ratingsRepo.getLikeRatingById(id);
    }

    public Optional<LikeRatings> getLikeRatingByUser(Long id_user, Long id_buku){
        return this.ratingsRepo.getLikeRatingByUser(id_user, id_buku);
    }

    public void saveRating(LikeRatings ratings){
        this.ratingsRepo.save(ratings);
    }
}

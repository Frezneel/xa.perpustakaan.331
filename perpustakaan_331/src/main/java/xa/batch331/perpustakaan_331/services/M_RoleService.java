package xa.batch331.perpustakaan_331.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xa.batch331.perpustakaan_331.models.M_Role;
import xa.batch331.perpustakaan_331.repositories.M_RoleRepo;

import java.util.List;
import java.util.Optional;

@Service
public class M_RoleService {

    @Autowired
    private M_RoleRepo m_roleRepo;

    public List<M_Role> getAllRole(){
        return this.m_roleRepo.findAll();
    }

    public Optional<M_Role> getRoleById(Long id){
        return this.m_roleRepo.findById(id);
    }

    public Optional<M_Role> validationNameCode(String name, String code){
        return this.m_roleRepo.validationNameCode(name, code);
    }

    public void saveRole(M_Role m_role){
        this.m_roleRepo.save(m_role);
    }

    public void deleteRoleById(Long id){
        this.m_roleRepo.deleteById(id);
    }
}

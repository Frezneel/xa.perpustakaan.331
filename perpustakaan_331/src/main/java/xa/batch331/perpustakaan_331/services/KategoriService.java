package xa.batch331.perpustakaan_331.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import xa.batch331.perpustakaan_331.models.Kategori;
import xa.batch331.perpustakaan_331.repositories.KategoriRepo;

import java.util.List;
import java.util.Optional;

@Service
public class KategoriService {

    @Autowired
    private KategoriRepo kategoriRepo;

    public List<Kategori> getAllKategori(){
        return this.kategoriRepo.getAllKategori();
    }

    public Page<Kategori> getAllKategori(Pageable pageable){
        return this.kategoriRepo.getAllKategori(pageable);
    }

    public Page<Kategori> searchQuery(Pageable pageable, String search_query){
        return this.kategoriRepo.searchQuery(pageable, search_query);
    }

    public Optional<Kategori> getKategoriById(Long id){
        return this.kategoriRepo.getKategoriById(id);
    }

    public Optional<Kategori> searchByNama(String nama){
        return this.kategoriRepo.searchByNama(nama);
    }

    public void saveKategori(Kategori kategori){
        this.kategoriRepo.save(kategori);
    }

    public void deleteKategori(Long id){
        this.kategoriRepo.deleteById(id);
    }

}

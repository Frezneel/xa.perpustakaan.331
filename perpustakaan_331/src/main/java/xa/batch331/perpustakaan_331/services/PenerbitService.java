package xa.batch331.perpustakaan_331.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import xa.batch331.perpustakaan_331.models.Penerbit;
import xa.batch331.perpustakaan_331.repositories.PenerbitRepo;

import java.util.List;
import java.util.Optional;

@Service
public class PenerbitService {
    @Autowired
    private PenerbitRepo penerbitRepo;

    public List<Penerbit> getAllPenerbit(){
        return this.penerbitRepo.getAllPenerbit();
    }

    public Page<Penerbit> getAllPenerbit(Pageable pageable){
        return this.penerbitRepo.getAllPenerbit(pageable);
    }

    public Page<Penerbit> searchQueryPenerbit(String search_query,Pageable pageable){
        return this.penerbitRepo.searchQueryPenerbit(search_query, pageable);
    }

    public Optional<Penerbit> getPenerbitById(Long id){
        return this.penerbitRepo.getPenerbitById(id);
    }

    public void savePenerbit(Penerbit penerbit){
        this.penerbitRepo.save(penerbit);
    }

    public void deletePenerbit(Long id){
        this.penerbitRepo.deleteById(id);
    }

    //validation
    public Optional<Penerbit> searchByNama(String nama){
        return this.penerbitRepo.searchByNama(nama);
    }
}

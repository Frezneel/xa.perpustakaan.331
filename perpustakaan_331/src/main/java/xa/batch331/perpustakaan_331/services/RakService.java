package xa.batch331.perpustakaan_331.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xa.batch331.perpustakaan_331.models.Rak;
import xa.batch331.perpustakaan_331.repositories.RakRepo;

import java.util.List;
import java.util.Optional;

@Service
public class RakService {
    @Autowired
    private RakRepo rakRepo;

    public List<Rak> getAllRak(){
        return this.rakRepo.getAllRak();
    }

    public Optional<Rak> getRakById(Long id){
        return this.rakRepo.getRakById(id);
    }

    public void saveRak(Rak rak){
        this.rakRepo.save(rak);
    }

    public void deleteRak(Long id){
        this.rakRepo.deleteById(id);
    }

    public Optional<Rak> searchByLokasi(String lokasi){
        return this.rakRepo.searchByLokasi(lokasi);
    }
}

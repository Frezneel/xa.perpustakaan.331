package xa.batch331.perpustakaan_331.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xa.batch331.perpustakaan_331.models.M_User;
import xa.batch331.perpustakaan_331.repositories.M_UserRepo;

import java.util.List;
import java.util.Optional;

@Service
public class M_UserService {

    @Autowired
    private M_UserRepo m_userRepo;

    public List<M_User> getAllUser(){
        return this.m_userRepo.getAllUser();
    }

    public Optional<M_User> getUserById(Long id){
        return this.m_userRepo.getUserById(id);
    }

    public Optional<M_User> loginValidation(String email, String password){
        return this.m_userRepo.checkEmailPassword(email, password);
    }

    public Optional<M_User> getUserByEmail(String email){
        return this.m_userRepo.validasiEmail(email);
    }

    public Optional<M_User> emailValidation(String email){
        return this.m_userRepo.validasiEmail(email);
    }

    public void saveUser(M_User m_user){
        this.m_userRepo.save(m_user);
    }
}

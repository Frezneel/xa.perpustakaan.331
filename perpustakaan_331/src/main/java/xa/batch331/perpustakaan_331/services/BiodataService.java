package xa.batch331.perpustakaan_331.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xa.batch331.perpustakaan_331.models.Biodata;
import xa.batch331.perpustakaan_331.repositories.BiodataRepo;

import java.util.List;
import java.util.Optional;

@Service
public class BiodataService {

    @Autowired
    private BiodataRepo biodataRepo;

    public List<Biodata> getAllBiodata(){
        return this.biodataRepo.getAllBiodata();
    }

    public Optional<Biodata> getBiodataById(Long id){
        return this.biodataRepo.getBiodataById(id);
    }

    public void saveBiodata(Biodata biodata){
        this.biodataRepo.save(biodata);
    }

}

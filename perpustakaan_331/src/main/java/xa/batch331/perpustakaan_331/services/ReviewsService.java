package xa.batch331.perpustakaan_331.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xa.batch331.perpustakaan_331.models.Reviews;
import xa.batch331.perpustakaan_331.repositories.ReviewsRepo;

import java.util.List;
import java.util.Optional;

@Service
public class ReviewsService {

    @Autowired
    private ReviewsRepo reviewsRepo;

    public List<Reviews> getAllReview(){
        return this.reviewsRepo.getAllReview();
    }

    public Optional<Reviews> getReviewById(Long id){
        return this.reviewsRepo.findById(id);
    }

    public Optional<Reviews> getReviewByUser(Long user_id, Long buku_id){
        return this.reviewsRepo.getReviewByUser(user_id, buku_id);
    }

    public void saveReview(Reviews reviews){
        this.reviewsRepo.save(reviews);
    }

}

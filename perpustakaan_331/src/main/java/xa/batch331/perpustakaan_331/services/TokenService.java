package xa.batch331.perpustakaan_331.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xa.batch331.perpustakaan_331.models.Token;
import xa.batch331.perpustakaan_331.repositories.TokenRepo;

import java.util.List;
import java.util.Optional;

@Service
public class TokenService {

    @Autowired
    private TokenRepo tokenRepo;

    public List<Token> getAllToken(){
        return this.tokenRepo.findAll();
    }

    public Optional<Token> getTokenById(Long id){
        return this.tokenRepo.findById(id);
    }

    public Optional<Token> getTokenByToken(String token){
        return this.tokenRepo.getTokenByToken(token);
    }

    public Optional<Token> validationToken(String email, String used_for, String token){
        return this.tokenRepo.validationToken(email, used_for, token);
    }

    public Optional<Token> getLastTokenByEmail(String email, String used_for){
        return this.tokenRepo.getLastTokenByEmail(email, used_for);
    }

    public void saveToken(Token token){
        this.tokenRepo.save(token);
    }

    public void deleteTokenById(Long id){
        this.tokenRepo.deleteById(id);
    }

}

package xa.batch331.perpustakaan_331.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import xa.batch331.perpustakaan_331.models.Buku;
import xa.batch331.perpustakaan_331.repositories.BukuRepo;

import java.util.List;
import java.util.Optional;

@Service
public class BukuService {

    @Autowired
    private BukuRepo bukuRepo;

    public List<Buku> getAllBuku(){
        return this.bukuRepo.getAllBuku();
    }

    public Optional<Buku> getBukuById(Long id){
        return this.bukuRepo.getBukuById(id);
    }

    public Optional<Buku> searchByJudul(String judul){
        return this.bukuRepo.searchByJudul(judul);
    }

    public Page<Buku> getAllBuku(Pageable pageable){
        return this.bukuRepo.getAllBuku(pageable);
    }

    public Page<Buku> searchQuery(Pageable pageable, String search_query){
        return this.bukuRepo.searchQuery(pageable, search_query);
    }

    public void saveBuku(Buku buku){
        this.bukuRepo.save(buku);
    }

    public void deleteBuku(Long id){
        this.bukuRepo.deleteById(id);
    }

}

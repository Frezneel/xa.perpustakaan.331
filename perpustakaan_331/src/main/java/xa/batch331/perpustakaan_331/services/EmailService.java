package xa.batch331.perpustakaan_331.services;

import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import xa.batch331.perpustakaan_331.dto.MailStructure;

@Service
@Transactional
public class EmailService {
    @Autowired
    private JavaMailSender javaMailSender;

    @Value("${spring.mail.username}")
    private String fromMail;

    public void sendOTP(String mail, MailStructure mailStructure){
        MimeMessage message = javaMailSender.createMimeMessage();
        MimeMessageHelper helper;

        try {
            helper = new MimeMessageHelper(message, true);
            helper.setTo(mail);
            helper.setFrom(fromMail);
            helper.setSubject(mailStructure.getSubject());
            helper.setText(mailStructure.getMessage(), true);
            javaMailSender.send(message);

        }catch (MessagingException e){
            e.printStackTrace();
        }
    }
}

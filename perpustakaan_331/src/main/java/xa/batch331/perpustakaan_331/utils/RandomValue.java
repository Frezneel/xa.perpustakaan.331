package xa.batch331.perpustakaan_331.utils;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import java.util.Random;

@Service
public class RandomValue {
    private final String HURUF_KAPITAL = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private final String HURUF_KECIL = "abcdefghijklmnopqrstuvwxyz";
    private final String ANGKA = "0123456789";
    private final String SIMBOL = "!@#$%^&*()-_+={}[];:,.<>/?";

    private final Random random = new Random();

    public String randomValue(Integer size, boolean onUppercase, boolean onLowercase,
                            boolean onNumbers, boolean onSymbols){
        String VALID_RANDOM = "";
        if (onUppercase) {VALID_RANDOM += HURUF_KAPITAL;}
        if (onLowercase) {VALID_RANDOM += HURUF_KECIL;}
        if (onNumbers) {VALID_RANDOM += ANGKA;}
        if (onSymbols) {VALID_RANDOM += SIMBOL;}
        String value = "";
        for (int i = 0; i < size; i++) {
            int randomIndex = random.nextInt(VALID_RANDOM.length());
            char randomChar = VALID_RANDOM.charAt(randomIndex);
            value += randomChar;
        }
        return value;
    }
}

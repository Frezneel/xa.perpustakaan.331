package xa.batch331.perpustakaan_331;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Perpustakaan331Application {

	public static void main(String[] args) {
		SpringApplication.run(Perpustakaan331Application.class, args);
	}

}

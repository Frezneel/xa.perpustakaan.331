package xa.batch331.perpustakaan_331.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import xa.batch331.perpustakaan_331.models.LikeRatings;

import java.util.List;
import java.util.Optional;

@Repository
public interface LikeRatingsRepo extends JpaRepository<LikeRatings, Long> {

    @Query(value = "SELECT like_ratings.* FROM like_ratings JOIN buku ON like_ratings.buku_id = buku.id " +
            "WHERE like_ratings.is_delete = false AND buku.id = :id",nativeQuery = true)
    List<LikeRatings> getLikeRatingByBuku (Long id);

    @Query(value = "SELECT * FROM like_ratings " +
            "WHERE like_ratings.is_delete = false AND like_ratings.id = :id",nativeQuery = true)
    Optional<LikeRatings> getLikeRatingById (Long id);

    @Query(value = "SELECT * FROM like_ratings " +
            "WHERE like_ratings.is_delete = false AND like_ratings.created_by = :id_user AND like_ratings.buku_id = :id_buku",nativeQuery = true)
    Optional<LikeRatings> getLikeRatingByUser (Long id_user, Long id_buku);



}

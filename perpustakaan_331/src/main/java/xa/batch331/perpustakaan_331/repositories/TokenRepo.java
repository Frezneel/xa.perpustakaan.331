package xa.batch331.perpustakaan_331.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import xa.batch331.perpustakaan_331.models.Token;

import java.util.Optional;

@Repository
public interface TokenRepo extends JpaRepository<Token, Long> {

    @Query(value = "SELECT * FROM token WHERE token.token = :token", nativeQuery = true)
    Optional<Token> getTokenByToken(String token);

    @Query(value = "SELECT * FROM token WHERE lower(token.email) = lower(:email) " +
            "AND lower(token.used_for) = lower(:used_for) ORDER BY token.expired_on DESC LIMIT 1", nativeQuery = true)
    Optional<Token> getLastTokenByEmail(String email, String used_for);

    @Query(value = "SELECT * FROM token WHERE lower(token.email) = lower(:email) " +
            "AND lower(token.used_for) = lower(:used_for) " +
            "AND token.token = :token ORDER BY token.expired_on DESC LIMIT 1", nativeQuery = true)
    Optional<Token> validationToken(String email, String used_for, String token);


}

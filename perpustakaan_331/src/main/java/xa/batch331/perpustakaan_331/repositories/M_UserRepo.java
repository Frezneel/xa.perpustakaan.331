package xa.batch331.perpustakaan_331.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import xa.batch331.perpustakaan_331.models.M_User;

import java.util.List;
import java.util.Optional;

@Repository
public interface M_UserRepo extends JpaRepository<M_User, Long> {

    @Query(value = "SELECT * FROM m_user WHERE m_user.is_delete = false", nativeQuery = true)
    List<M_User> getAllUser();

    @Query(value = "SELECT * FROM m_user WHERE m_user.is_delete = false " +
            "AND m_user.id = :id", nativeQuery = true)
    Optional<M_User> getUserById(Long id);

    @Query(value = "SELECT * FROM m_user WHERE m_user.is_delete = false " +
            "AND m_user.email = :email AND m_user.password = :password", nativeQuery = true)
    Optional<M_User> checkEmailPassword(String email, String password);

    @Query(value = "SELECT * FROM m_user WHERE m_user.is_delete = false " +
            "AND m_user.email LIKE lower(concat('%',:email,'%'))", nativeQuery = true)
    Optional<M_User> validasiEmail(String email);
}

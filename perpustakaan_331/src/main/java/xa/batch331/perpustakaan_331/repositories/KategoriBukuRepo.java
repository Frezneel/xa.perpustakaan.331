package xa.batch331.perpustakaan_331.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import xa.batch331.perpustakaan_331.models.KategoriBuku;

import java.util.List;
import java.util.Optional;

public interface KategoriBukuRepo extends JpaRepository<KategoriBuku, Long> {

    @Query(value = "SELECT * FROM kategori_buku WHERE kategori_buku.is_delete = false", nativeQuery = true)
    List<KategoriBuku> getAllBuku();

    @Query(value = "SELECT * FROM kategori_buku WHERE kategori_buku.is_delete = false AND kategori_buku.id = :id", nativeQuery = true)
    Optional<KategoriBuku> getBukuById(Long id);

    @Query(value = "SELECT * FROM kategori_buku WHERE kategori_buku.is_delete = false AND kategori_buku.buku_id = :buku_id " +
            "AND kategori_buku.kategori_id = :kategori_id", nativeQuery = true)
    Optional<KategoriBuku> saveValidation(Long buku_id, Long kategori_id);
}

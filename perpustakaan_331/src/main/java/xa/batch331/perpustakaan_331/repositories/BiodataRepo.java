package xa.batch331.perpustakaan_331.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import xa.batch331.perpustakaan_331.models.Biodata;

import java.util.List;
import java.util.Optional;

@Repository
public interface BiodataRepo extends JpaRepository<Biodata, Long> {

    @Query(value = "SELECT * FROM biodata WHERE biodata.is_delete = false", nativeQuery = true)
    List<Biodata> getAllBiodata();

    @Query(value = "SELECT * FROM biodata WHERE biodata.is_delete = false " +
            "AND biodata.id = :id", nativeQuery = true)
    Optional<Biodata> getBiodataById(Long id);

}

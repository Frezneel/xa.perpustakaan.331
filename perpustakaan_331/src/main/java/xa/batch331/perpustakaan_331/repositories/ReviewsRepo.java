package xa.batch331.perpustakaan_331.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import xa.batch331.perpustakaan_331.models.Reviews;

import java.util.List;
import java.util.Optional;

@Repository
public interface ReviewsRepo extends JpaRepository<Reviews, Long> {

    @Query(value = "SELECT * FROM reviews WHERE reviews.is_delete = false", nativeQuery = true)
    List<Reviews> getAllReview();

    @Query(value = "SELECT * FROM reviews " +
            "WHERE reviews.is_delete = false AND reviews.created_by = :user_id AND reviews.buku_id = :buku_id", nativeQuery = true)
    Optional<Reviews> getReviewByUser(Long user_id, Long buku_id);

}

package xa.batch331.perpustakaan_331.repositories;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import xa.batch331.perpustakaan_331.models.Buku;

import java.util.List;
import java.util.Optional;

@Repository
public interface BukuRepo extends JpaRepository<Buku, Long> {

    @Query(value = "SELECT * FROM buku WHERE buku.is_delete = false", nativeQuery = true)
    List<Buku> getAllBuku();

    @Query(value = "SELECT * FROM buku WHERE buku.is_delete = false", nativeQuery = true)
    Page<Buku> getAllBuku(Pageable pageable);

    @Query(value = "SELECT buku.* FROM buku " +
            "JOIN pengarang ON pengarang.id = buku.pengarang_id " +
            "JOIN penerbit ON penerbit.id = buku.penerbit_id " +
            "JOIN rak ON rak.id = buku.rak_id " +
            "WHERE buku.is_delete = false " +
            "AND (lower(buku.judul) LIKE lower(concat('%', :search_query, '%')) " +
            "OR lower(buku.isbn) LIKE lower(concat('%', :search_query, '%')) " +
            "OR lower(pengarang.nama) LIKE lower(concat('%', :search_query, '%')) " +
            "OR lower(penerbit.nama) LIKE lower(concat('%', :search_query, '%')) " +
            "OR lower(rak.lokasi) LIKE lower(concat('%', :search_query, '%')))",nativeQuery = true)
    Page<Buku> searchQuery(Pageable pageable, String search_query);

    @Query(value = "SELECT * FROM buku WHERE buku.is_delete = false AND buku.id = :id", nativeQuery = true)
    Optional<Buku> getBukuById(Long id);

    @Query(value = "SELECT * FROM buku WHERE buku.is_delete = false AND lower(buku.judul) = lower(:judul)", nativeQuery = true)
    Optional<Buku> searchByJudul(String judul);
    //Pagination

}

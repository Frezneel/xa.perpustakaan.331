package xa.batch331.perpustakaan_331.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import xa.batch331.perpustakaan_331.models.Kategori;

import java.util.List;
import java.util.Optional;

@Repository
public interface KategoriRepo extends JpaRepository<Kategori, Long> {
    @Query(value = "SELECT * FROM kategori WHERE kategori.is_delete = false", nativeQuery = true)
    List<Kategori> getAllKategori();

    @Query(value = "SELECT * FROM kategori WHERE kategori.is_delete = false", nativeQuery = true)
    Page<Kategori> getAllKategori(Pageable pageable);

    @Query(value = "SELECT * FROM kategori WHERE kategori.is_delete = false " +
            "AND lower(kategori.nama) LIKE lower(concat('%',:search_query,'%'))", nativeQuery = true)
    Page<Kategori> searchQuery(Pageable pageable, String search_query);

    @Query(value = "SELECT * FROM kategori WHERE kategori.is_delete = false AND kategori.id = :id", nativeQuery = true)
    Optional<Kategori> getKategoriById(Long id);

    @Query(value = "SELECT * FROM kategori WHERE kategori.is_delete = false AND lower(kategori.nama) = lower(:nama)", nativeQuery = true)
    Optional<Kategori> searchByNama(String nama);
}

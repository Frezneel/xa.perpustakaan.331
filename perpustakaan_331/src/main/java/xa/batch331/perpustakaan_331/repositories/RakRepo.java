package xa.batch331.perpustakaan_331.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import xa.batch331.perpustakaan_331.models.Rak;

import java.util.List;
import java.util.Optional;

@Repository
public interface RakRepo extends JpaRepository<Rak, Long> {

    @Query(value = "SELECT * FROM rak WHERE rak.is_delete = false", nativeQuery = true)
    List<Rak> getAllRak();

    @Query(value = "SELECT * FROM rak WHERE rak.is_delete = false AND rak.id = :id", nativeQuery = true)
    Optional<Rak> getRakById(Long id);

    @Query(value = "SELECT * FROM rak WHERE rak.is_delete = false AND lower(rak.lokasi) = lower(:lokasi)",nativeQuery = true)
    Optional<Rak> searchByLokasi(String lokasi);

}

package xa.batch331.perpustakaan_331.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import xa.batch331.perpustakaan_331.models.Penerbit;

import java.util.List;
import java.util.Optional;

@Repository
public interface PenerbitRepo extends JpaRepository<Penerbit, Long>{
    @Query(value = "SELECT * FROM penerbit WHERE penerbit.is_delete = false", nativeQuery = true)
    List<Penerbit> getAllPenerbit();

    //pagination
    @Query(value = "SELECT * FROM penerbit WHERE penerbit.is_delete = false", nativeQuery = true)
    Page<Penerbit> getAllPenerbit(Pageable pageable);

    //Search_function
    @Query(value = "SELECT * FROM penerbit WHERE penerbit.is_delete = false " +
            "AND lower(penerbit.nama) LIKE lower(concat('%',:search_query,'%'))",nativeQuery = true)
    Page<Penerbit> searchQueryPenerbit(String search_query, Pageable pageable);

    @Query(value = "SELECT * FROM penerbit WHERE penerbit.is_delete = false AND penerbit.id = :id", nativeQuery = true)
    Optional<Penerbit> getPenerbitById(Long id);

    //search_validation
    @Query(value = "SELECT * FROM penerbit WHERE penerbit.is_delete = false AND lower(penerbit.nama) = lower(:nama)", nativeQuery = true)
    Optional<Penerbit> searchByNama(String nama);


}

package xa.batch331.perpustakaan_331.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import xa.batch331.perpustakaan_331.models.M_Role;

import java.util.Optional;

@Repository
public interface M_RoleRepo extends JpaRepository<M_Role, Long> {

    @Query(value = "SELECT * FROM m_role WHERE m_role.is_delete = false " +
            "AND (lower(m_role.name) = lower(concat('%', :name, '%')) " +
            "OR lower(m_role.code) = lower(concat('%', :code, '%')) )", nativeQuery = true)
    Optional<M_Role> validationNameCode(String name, String code);

}

package xa.batch331.perpustakaan_331.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import xa.batch331.perpustakaan_331.models.Kategori;
import xa.batch331.perpustakaan_331.services.KategoriService;

import java.util.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class KategoriRestController {

    @Autowired
    private KategoriService kategoriService;
    private Map<String, Object> result;

    @GetMapping("/kategori")
    public ResponseEntity<List<Kategori>> getAllKategori(){
        try {
            List<Kategori> kategoriList = this.kategoriService.getAllKategori();
            return new ResponseEntity<>(kategoriList, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/kategori/map")
    public ResponseEntity<Object> getAllKategori(@RequestParam(defaultValue = "1") Integer page,
                                                 @RequestParam(defaultValue = "5") Integer size){
        try {
            Pageable pageable = PageRequest.of((page-1), size);
            Page<Kategori> pageTuts = this.kategoriService.getAllKategori(pageable);
            List<Kategori> dataKategori = pageTuts.getContent();

            result = new HashMap<>();
            result.put("dataApi", dataKategori);
            result.put("halamanSekarang", (pageTuts.getNumber()+1));
            result.put("totalHalaman", pageTuts.getTotalPages());
            result.put("banyakData", pageTuts.getTotalElements());

            return new ResponseEntity<>(result, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @GetMapping("/kategori/result")
    public ResponseEntity<Object> getAllKategori(@RequestParam String search_query,
                                                 @RequestParam(defaultValue = "1") Integer page,
                                                 @RequestParam(defaultValue = "5") Integer size){
        try {
            Pageable pageable = PageRequest.of((page-1), size);
            Page<Kategori> pageTuts = this.kategoriService.searchQuery(pageable, search_query);
            List<Kategori> dataKategori = pageTuts.getContent();

            result = new HashMap<>();
            result.put("dataApi", dataKategori);
            result.put("halamanSekarang", (pageTuts.getNumber()+1));
            result.put("totalHalaman", pageTuts.getTotalPages());
            result.put("banyakData", pageTuts.getTotalElements());

            return new ResponseEntity<>(result, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/kategori/{id}")
    public ResponseEntity<Optional<Kategori>> getKategoriById(@PathVariable("id")Long id){
        try {
            Optional<Kategori> kategori = this.kategoriService.getKategoriById(id);
            if (kategori.isPresent()){
                return new ResponseEntity<>(kategori, HttpStatus.OK);
            }else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/kategori/{id}")
    public ResponseEntity<?> updateKategori(@RequestBody Kategori kategori, @PathVariable("id")Long id){
        try {
            Optional<Kategori> kategori_data = this.kategoriService.getKategoriById(id);
            Optional<Kategori> cek_nama = this.kategoriService.searchByNama(kategori.getNama().trim());
            if (kategori_data.isPresent() && cek_nama.isEmpty()){
                kategori.setId(id);
                kategori.setCreated_by(kategori_data.get().getCreated_by());
                kategori.setCreated_on(kategori_data.get().getCreated_on());
                kategori.setModified_by(1L);
                kategori.setModified_on(new Date());
                result.put("status", "belum");
                result.put("data", kategori);
                this.kategoriService.saveKategori(kategori);
                return new ResponseEntity<>(result, HttpStatus.OK);
            } else if (kategori_data.isPresent() && cek_nama.isPresent()) {
                result.put("status", "ada");
                return new ResponseEntity<>(result, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/kategori/{id}")
    public ResponseEntity<?> deleteKategori (@PathVariable Long id){
        try {
            Optional<Kategori> kategori = this.kategoriService.getKategoriById(id);
            if (kategori.isPresent()){
                kategori.get().setDeleted_by(1L);
                kategori.get().setDeleted_on(new Date());
                kategori.get().setIs_delete(true);
                this.kategoriService.saveKategori(kategori.get());
                Map<String, Object> result = new HashMap<>();
                result.put("status", "sukses");
                return new ResponseEntity<>(result, HttpStatus.OK);
            }else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/kategori")
    public ResponseEntity<Object> addKategori(@RequestBody Kategori kategori){
        try {
            Optional<Kategori> data =this.kategoriService.searchByNama(kategori.getNama().trim());
            result = new HashMap<>();
            if (data.isPresent()){
                result.put("status", "ada");
                return new ResponseEntity<>(result ,HttpStatus.OK);
            }else {
                kategori.setCreated_on(new Date());
                kategori.setCreated_by(1L);
                this.kategoriService.saveKategori(kategori);
                result.put("status", "belum");
                result.put("data", kategori);
                return new ResponseEntity<>(result, HttpStatus.OK);
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}

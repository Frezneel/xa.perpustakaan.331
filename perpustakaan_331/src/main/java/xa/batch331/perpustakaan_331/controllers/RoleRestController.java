package xa.batch331.perpustakaan_331.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import xa.batch331.perpustakaan_331.models.M_Role;
import xa.batch331.perpustakaan_331.services.M_RoleService;

import java.util.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class RoleRestController {

    @Autowired
    private M_RoleService m_roleService;
    private Map<String, Object> result;

    @GetMapping("/role")
    public ResponseEntity<List<M_Role>> getAllRole(){
        try {
            List<M_Role> m_roles = this.m_roleService.getAllRole();
            return new ResponseEntity<>(m_roles, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/role/{id}")
    public ResponseEntity<Object> getRoleById(@PathVariable("id") Long id){
        try {
            Optional<M_Role> m_role = this.m_roleService.getRoleById(id);
            result = new HashMap<>();
            if (m_role.isPresent()){
                result.put("info", "found");
                result.put("data", m_role);
                return new ResponseEntity<>(result, HttpStatus.OK);
            }else {
                result.put("info", "empty");
                return new ResponseEntity<>(result, HttpStatus.OK);
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/role/{id}")
    public ResponseEntity<Object> putRoleById(@PathVariable("id")Long id, @RequestBody M_Role m_role){
        try {
            Optional<M_Role> validation = this.m_roleService.getRoleById(id);
            result = new HashMap<>();
            if (validation.isPresent()){
                m_role.setId(id);
                this.m_roleService.saveRole(m_role);
                result.put("info", "found");
                result.put("data", m_role);
                return new ResponseEntity<>(result, HttpStatus.OK);
            }else {
                result.put("info", "empty");
                return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/role/{id}")
    public ResponseEntity<Object> deleteRoleById(@PathVariable("id") Long id){
        try {
            Optional<M_Role> m_role = this.m_roleService.getRoleById(id);
            result = new HashMap<>();
            if (m_role.isPresent()){
                m_role.get().setModified_on(new Date());
                m_role.get().setModified_by(1L);
                m_role.get().setIs_delete(true);

                result.put("info", "found");
                result.put("data", m_role);
                return new ResponseEntity<>(result, HttpStatus.OK);
            }else {
                result.put("info", "empty");
                return new ResponseEntity<>(result, HttpStatus.OK);
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/role")
    public ResponseEntity<Object> saveRole(@RequestBody M_Role m_role){
        try {
            Optional<M_Role> validation = this.m_roleService.validationNameCode(m_role.getName(), m_role.getCode());
            result = new HashMap<>();
            if (validation.isEmpty()){
                m_role.setCreated_by(1L);
                m_role.setCreated_on(new Date());
                this.m_roleService.saveRole(m_role);
                result.put("info", "empty");
                result.put("data", m_role);
                return new ResponseEntity<>(result, HttpStatus.OK);
            }else {
                result.put("info", "found");
                return new ResponseEntity<>(result, HttpStatus.OK);
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}

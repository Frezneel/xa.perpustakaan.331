package xa.batch331.perpustakaan_331.controllers;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import xa.batch331.perpustakaan_331.models.Buku;
import xa.batch331.perpustakaan_331.repositories.BukuRepo;
import xa.batch331.perpustakaan_331.services.BukuService;
import xa.batch331.perpustakaan_331.utils.RandomValue;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class BukuRestController {

    @Autowired
    private BukuService bukuService;

    @Autowired
    private RandomValue randomValue;

    private Map<String, Object> result;
    @Autowired
    private BukuRepo bukuRepo;

    private final static String UPLOADED_FOLDER = "D:/Belajar Cloud + project perpus/cloud_real/data/";

    @GetMapping("/buku")
    public ResponseEntity<List<Buku>> getAllBuku(){
        try {
            List<Buku> bukuList = this.bukuService.getAllBuku();
            return new ResponseEntity<>(bukuList, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/buku/map")
    public ResponseEntity<Object> getAllBuku(@RequestParam(defaultValue = "1") Integer page,
                                             @RequestParam(defaultValue = "5") Integer size){
        try {
            Pageable pageable = PageRequest.of((page-1), size);
            Page<Buku> pageTuts = this.bukuService.getAllBuku(pageable);
            List<Buku> dataBuku = pageTuts.getContent();
            result = new HashMap<>();
            result.put("dataApi", dataBuku);
            result.put("halamanSekarang", (pageTuts.getNumber()+1));
            result.put("totalHalaman", pageTuts.getTotalPages());
            result.put("banyakData", pageTuts.getTotalElements());

            return new ResponseEntity<>(result, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @GetMapping("/buku/result")
    public ResponseEntity<Object> searchQueryBuku(@RequestParam String search_query,
                                                  @RequestParam(defaultValue = "1") Integer page,
                                                  @RequestParam(defaultValue = "5") Integer size){
        try {
            Pageable pageable = PageRequest.of((page-1), size);
            Page<Buku> pageTuts = this.bukuService.searchQuery(pageable, search_query);
            List<Buku> dataBuku = pageTuts.getContent();
            result = new HashMap<>();
            result.put("dataApi", dataBuku);
            result.put("halamanSekarang", (pageTuts.getNumber()+1));
            result.put("totalHalaman", pageTuts.getTotalPages());
            result.put("banyakData", pageTuts.getTotalElements());

            return new ResponseEntity<>(result, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/buku/{id}")
    public ResponseEntity<Optional<Buku>> getBukuById(@PathVariable("id")Long id){
        try {
            Optional<Buku> buku = this.bukuService.getBukuById(id);
            if (buku.isPresent()){
                return new ResponseEntity<>(buku, HttpStatus.OK);
            }else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/buku/{id}")
    public ResponseEntity<?> updateBuku(@RequestBody Buku buku, @PathVariable("id")Long id){
        try {
            result = new HashMap<>();
            Optional<Buku> buku_data = this.bukuService.getBukuById(id);
            Optional<Buku> validasi_judul = this.bukuService.searchByJudul(buku.getJudul().trim());
            if (buku_data.isPresent() && validasi_judul.isEmpty()){
                buku.setId(id);
                buku.setCreated_by(buku_data.get().getCreated_by());
                buku.setCreated_on(buku_data.get().getCreated_on());
                buku.setModified_by(1L);
                buku.setModified_on(new Date());
                this.bukuService.saveBuku(buku);
                result.put("status", "belum");
                result.put("data", buku);
                return new ResponseEntity<>(result, HttpStatus.OK);
            } else if (buku_data.isPresent() && validasi_judul.isPresent()) {
                result.put("status", "ada");
                result.put("detail", "Judul yang digunakan terdapat di database");
                return new ResponseEntity<>(result, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/buku/{id}")
    public ResponseEntity<?> deleteBuku (@PathVariable Long id){
        try {
            Optional<Buku> buku = this.bukuService.getBukuById(id);
            if (buku.isPresent()){
                buku.get().setDeleted_by(1L);
                buku.get().setDeleted_on(new Date());
                buku.get().setIs_delete(true);
                this.bukuService.saveBuku(buku.get());
                Map<String, Object> result = new HashMap<>();
                result.put("status", "sukses");
                return new ResponseEntity<>(result, HttpStatus.OK);
            }else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/buku")
    public ResponseEntity<Object> addBuku(@RequestBody Buku buku){
        try {
            result = new HashMap<>();
            Optional<Buku> data = this.bukuService.searchByJudul(buku.getJudul().trim());
            if (!data.isPresent()){
                buku.setCreated_on(new Date());
                buku.setCreated_by(1L);
                this.bukuService.saveBuku(buku);
                result.put("status", "belum");
                result.put("data", buku);
                return new ResponseEntity<>(result, HttpStatus.OK);
            }else {
                result.put("status", "ada");
                result.put("detail", "Judul yang digunakan terdapat di database");
                return new ResponseEntity<>(result, HttpStatus.OK);

            }

        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping(value = "/buku/addImage/", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<Object> saveImageUser(@RequestParam("id") Long id, @RequestParam("filepath") MultipartFile file){
        try {
            Optional<Buku> buku = this.bukuService.getBukuById(id);
            if (buku.isPresent()){
                byte[] itemByte = file.getBytes();
                String newFileName = UUID.randomUUID().toString() + "." + FilenameUtils.getExtension(file.getOriginalFilename());
                buku.get().setImage_file(itemByte);
                buku.get().setImage_path("/files/" + newFileName);
                buku.get().setModified_on(new Date());
                buku.get().setModified_by(1L);
                buku.get().setCreated_by(buku.get().getCreated_by());
                buku.get().setCreated_on(buku.get().getCreated_on());
                this.bukuService.saveBuku(buku.get());

                Path path = Paths.get(UPLOADED_FOLDER + newFileName);
                Files.write(path, itemByte);
                return new ResponseEntity<>("Disimpan", HttpStatus.OK);
            }else {
                return new ResponseEntity<>("Gagal", HttpStatus.OK);
            }
        }catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}

package xa.batch331.perpustakaan_331.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import xa.batch331.perpustakaan_331.dto.MailStructure;
import xa.batch331.perpustakaan_331.services.EmailService;
import xa.batch331.perpustakaan_331.utils.RandomValue;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/v2")
public class EmailRestController {

    @Autowired
    private EmailService emailService;
    private Map<String, Object> result;

    @Autowired
    private RandomValue randomValue;

    private final String TEMPLATE_HEADER_MESSAGE = "<!DOCTYPE html><html> <head> <style>" +
            "body {font-family: Arial, sans-serif; }" +
            ".container { max-width: 600px; margin: 0 auto; padding: 20px; border: 1px solid #ccc; border-radius: 5px;}" +
            ".otp { font-size: 24px; font-weight: bold; text-align: center; margin-bottom: 20px;}" +
            ".instructions { margin-bottom: 20px; }" +
            ".button { display: inline-block; padding: 10px 20px; background-color: #4CAF50; color: white; text-align: center;" +
            "text-decoration: none; border-radius: 4px;}" +
            ".center{text-align: center;} </style> </head>" +
            "<body>";
    private final String TEMPLATE_BODY_OTP_MESSAGE = "<div class=\"container\"> <div class=\"instructions center\">" +
            "OTP ini bersifat RAHASIA harap tidak dibagikan ke siapapun. Silakan masukan kode ini untuk pendaftaran kamu." +
            "</div>";
    private final String TEMPLATE_BODY_FORGOTPASSWORD_MESSAGE = "<div class=\"container\"> <div class=\"instructions center\">" +
            "OTP ini bersifat RAHASIA harap tidak dibagikan ke siapapun. Silakan masukan kode ini untuk lupa password kamu." +
            "</div>";
    private final String TEMPLATE_FOOTER_MESSAGE = "<div class=\"instructions center\">" +
            "Jika merasa tidak melakukan pendaftaran abaikan mail ini.</div>" +
            "<div class=\"center\">" +
            "<button class=\"button\" href=\"#\" disabled>Terima Kasih</button>" +
            "</div> </div> </body> </html>";

    @PostMapping("/mail/otp/register")
    public ResponseEntity<Object> getMailOTP(@RequestParam String mailTo){
        try {
            MailStructure mailStructure = new MailStructure();
            String code_otp = this.randomValue.randomValue(6, true, true,true, false);
            final String MESSAGE_CODE = "<div class=\"otp\"> Kode OTP: " + code_otp + "</div>";
            String MESSAGE = TEMPLATE_HEADER_MESSAGE + TEMPLATE_BODY_OTP_MESSAGE + MESSAGE_CODE + TEMPLATE_FOOTER_MESSAGE;
            mailStructure.setSubject("OTP Register PIN");
            mailStructure.setMessage(MESSAGE);
            this.emailService.sendOTP(mailTo, mailStructure);
            result = new HashMap<>();
            result.put("status", "success");
            result.put("otp", code_otp);
            return new ResponseEntity<>(result, HttpStatus.OK);
        }catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

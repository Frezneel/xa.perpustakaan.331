package xa.batch331.perpustakaan_331.controllers;

import jakarta.servlet.http.HttpSession;
import jakarta.websocket.server.PathParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import xa.batch331.perpustakaan_331.dto.LoginStructure;
import xa.batch331.perpustakaan_331.models.M_User;
import xa.batch331.perpustakaan_331.services.M_UserService;

import java.util.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class UserRestController {

    @Autowired
    private M_UserService m_userService;
    private Map<String, Object> result;

    @GetMapping("/user/session")
    public ResponseEntity<Object> sessionLogin(HttpSession session){
        try {
            Long user_id = (Long) session.getAttribute("user_id");
            Long role_id = (Long) session.getAttribute("role_id");

            result = new HashMap<>();
            if (user_id != null){
                result.put("user_id", user_id);
                result.put("role_id", role_id);
                result.put("status", "logged_in");
                return new ResponseEntity<>(result, HttpStatus.OK);
            }else {
                result.put("user_id", null);
                result.put("role_id", null);
                result.put("status", "empty");
                return new ResponseEntity<>(result, HttpStatus.OK);
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/user")
    public ResponseEntity<List<M_User>> getAllUsers(){
        try {
            List<M_User> m_users = this.m_userService.getAllUser();
            return new ResponseEntity<>(m_users, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/user/{id}")
    public ResponseEntity<Object> getUserById(@PathVariable("id")Long id){
        try {
            Optional<M_User> m_user = this.m_userService.getUserById(id);
            if (m_user.isPresent()){
                return new ResponseEntity<>(m_user, HttpStatus.OK);
            }else {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/user/{id}")
    public ResponseEntity<Object> putUserById(@PathVariable("id")Long id, @RequestBody M_User m_user){
        try {
            Optional<M_User> validasi_user = this.m_userService.getUserById(id);
            if (validasi_user.isPresent()){
                m_user.setId(id);
                m_user.setModified_by(id);
                m_user.setModified_on(new Date());
                m_user.setCreated_by(validasi_user.get().getCreated_by());
                m_user.setCreated_on(validasi_user.get().getCreated_on());
                this.m_userService.saveUser(m_user);
                return new ResponseEntity<>(HttpStatus.OK);
            }else {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/user/mail={email}")
    public ResponseEntity<Object> findUserByEmail(@PathVariable String email){
        try {
            Optional<M_User> data = this.m_userService.getUserByEmail(email);
            result = new HashMap<>();
            if (data.isPresent()){
                result.put("status", "found");
                return new ResponseEntity<>(result, HttpStatus.OK);
            }else {
                result.put("status", "empty");
                return new ResponseEntity<>(result, HttpStatus.OK);
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/user")
    public ResponseEntity<Object> saveUser(@RequestBody M_User m_user){
        try {
            result = new HashMap<>();
            Optional<M_User> validasiData = this.m_userService.emailValidation(m_user.getEmail());
            if (validasiData.isEmpty()){
                m_user.setCreated_on(new Date());
                m_user.setCreated_by(1L);
                m_user.setIs_locked(false);
                m_userService.saveUser(m_user);
                m_user.setCreated_by(m_user.getId());
                m_userService.saveUser(m_user);

                result.put("status", "success");
                result.put("data", m_user);
                return new ResponseEntity<>(result, HttpStatus.OK);
            }else {
                result.put("status", "invalid");
                result.put("deskripsi", "Email telah digunakan");
                return new ResponseEntity<>(result, HttpStatus.OK);
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/user/login")
    public ResponseEntity<Object> loginUser(@RequestBody LoginStructure loginStructure, HttpSession session){
        try {
            Optional<M_User> data = this.m_userService.loginValidation(loginStructure.getEmail(), loginStructure.getPassword());
            result = new HashMap<>();
            if (data.isPresent()){
                Boolean isLocked = data.get().getIs_locked() == null ? false : data.get().getIs_locked();
                if (!isLocked){
                    session.setAttribute("user_id", data.get().getId());
                    session.setAttribute("role_id", data.get().getRole_id());
                    data.get().setModified_by(data.get().getId());
                    data.get().setModified_on(new Date());
                    data.get().setLast_login(new Date());
                    data.get().setLogin_attempt(null);
                    this.m_userService.saveUser(data.get());
                    result.put("info", "success");
                    return new ResponseEntity<>(result, HttpStatus.OK);
                }else {
                    result.put("info", "locked");
                    return new ResponseEntity<>(result, HttpStatus.OK);
                }

            }else {
                Optional<M_User> email_data = this.m_userService.getUserByEmail(loginStructure.getEmail());
                if (email_data.isPresent()){
                    Integer loginAttempt = email_data.get().getLogin_attempt() == null ? 0 : email_data.get().getLogin_attempt();
                    loginAttempt = loginAttempt < 3 ? loginAttempt + 1 : loginAttempt;
                    if (loginAttempt >= 3){
                        M_User userLock = email_data.get();
                        userLock.setIs_locked(true);
                        email_data.get().setLogin_attempt(loginAttempt);
                        this.m_userService.saveUser(userLock);
                    }else {
                        email_data.get().setLogin_attempt(loginAttempt);
                        this.m_userService.saveUser(email_data.get());
                    }
                }
                result.put("info", "failed");
                return new ResponseEntity<>(result, HttpStatus.OK);
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/user/logout")
    public ResponseEntity<Object> seesionLogout(HttpSession session){
        session.invalidate();
        result.put("info", "success");
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

}

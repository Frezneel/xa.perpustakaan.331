package xa.batch331.perpustakaan_331.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import xa.batch331.perpustakaan_331.models.LikeRatings;
import xa.batch331.perpustakaan_331.services.LikeRatingsService;

import java.util.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class LikeRatingsRestController {

    @Autowired
    private LikeRatingsService likeRatingsService;

    private Map<String, Object> result;

    @GetMapping("/like_rating")
    public ResponseEntity<List<LikeRatings>> getAllLikeRating(){
        try {
            List<LikeRatings> likeRatings = this.likeRatingsService.getAllRating();
            return new ResponseEntity<>(likeRatings, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/like_rating/result")
    public ResponseEntity<Object> getLikeRatingByBuku(@RequestParam Long id){
        try {
            List<LikeRatings> likeRatings = this.likeRatingsService.getLikeRatingByBuku(id);
            Long like = likeRatings.stream().filter(item -> item.getLiked() == true).count();
            Double rating = likeRatings.stream().map(item -> item.getRating()).filter(item -> item != null).reduce(0.0, (ans, i) -> ans + i)
                    / likeRatings.stream().map(item -> item.getRating()).filter(item -> item != null).count();

            result = new HashMap<>();
            result.put("info", "success");
            result.put("like_data", like);
            result.put("rating_data", rating);
            result.put("dataApi", likeRatings);
            return new ResponseEntity<>(result, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/like_rating")
    public ResponseEntity<Object> saveLikeRating(@RequestParam String rate,@RequestBody LikeRatings likeRatings){
        try {
            Optional<LikeRatings> validation = this.likeRatingsService.getLikeRatingByUser(likeRatings.getCreated_by(), likeRatings.getBuku_id());
            if (rate.toLowerCase().equals("like") && likeRatings.getLiked() != null){
                if (validation.isEmpty()){
                    likeRatings.setCreated_on(new Date());
                    this.likeRatingsService.saveRating(likeRatings);
                    return new ResponseEntity<>(likeRatings, HttpStatus.OK);
                }else {
                    likeRatings.setId(validation.get().getId());
                    likeRatings.setCreated_by(validation.get().getCreated_by());
                    likeRatings.setCreated_on(validation.get().getCreated_on());
                    likeRatings.setModified_by(validation.get().getCreated_by());
                    likeRatings.setModified_on(new Date());
                    likeRatings.setRating(validation.get().getRating());
                    this.likeRatingsService.saveRating(likeRatings);
                    return new ResponseEntity<>(likeRatings, HttpStatus.OK);
                }
            } else if (rate.toLowerCase().equals("rating") && likeRatings.getRating() != null) {
                if (validation.isEmpty()){
                    likeRatings.setCreated_on(new Date());
                    this.likeRatingsService.saveRating(likeRatings);
                    return new ResponseEntity<>(likeRatings, HttpStatus.OK);
                }else {
                    likeRatings.setId(validation.get().getId());
                    likeRatings.setCreated_by(validation.get().getCreated_by());
                    likeRatings.setCreated_on(validation.get().getCreated_on());
                    likeRatings.setModified_by(validation.get().getCreated_by());
                    likeRatings.setModified_on(new Date());
                    likeRatings.setLiked(validation.get().getLiked());
                    this.likeRatingsService.saveRating(likeRatings);
                    return new ResponseEntity<>(likeRatings, HttpStatus.OK);
                }
            }else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

package xa.batch331.perpustakaan_331.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import xa.batch331.perpustakaan_331.models.Biodata;
import xa.batch331.perpustakaan_331.services.BiodataService;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class BiodataRestController {

    @Autowired
    private BiodataService biodataService;

    private Map<String, Object> result;


    @PostMapping("/biodata")
    public ResponseEntity<Object> saveBiodata(@RequestBody Biodata biodata){
        try {
            biodata.setCreated_by(1L);
            biodata.setCreated_on(new Date());
            this.biodataService.saveBiodata(biodata);
            biodata.setCreated_by(biodata.getId());
            this.biodataService.saveBiodata(biodata);

            result = new HashMap<>();
            result.put("info", "success");
            result.put("data", biodata);
            return new ResponseEntity<>(result, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}

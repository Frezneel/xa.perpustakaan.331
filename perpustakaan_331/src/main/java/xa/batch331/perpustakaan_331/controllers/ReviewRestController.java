package xa.batch331.perpustakaan_331.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import xa.batch331.perpustakaan_331.models.Reviews;
import xa.batch331.perpustakaan_331.services.ReviewsService;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ReviewRestController {

    @Autowired
    private ReviewsService reviewsService;

    private Map<String, Object> result;

    @GetMapping("/review")
    public ResponseEntity<List<Reviews>> getAllReviews(){
        try {
            List<Reviews> reviews = this.reviewsService.getAllReview();
            return new ResponseEntity<>(reviews, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/review")
    public ResponseEntity<Object> saveReview(@RequestBody Reviews reviews){
        try {
            Optional<Reviews> validation = this.reviewsService.getReviewByUser(reviews.getCreated_by(), reviews.getBuku_id());
            if (validation.isEmpty()){
                reviews.setCreated_on(new Date());
                this.reviewsService.saveReview(reviews);
                return new ResponseEntity<>(reviews, HttpStatus.OK);
            }else {
                reviews.setId(validation.get().getId());
                reviews.setModified_by(validation.get().getId());
                reviews.setModified_on(new Date());
                reviews.setCreated_on(validation.get().getCreated_on());
                reviews.setCreated_by(validation.get().getCreated_by());
                this.reviewsService.saveReview(reviews);
                return new ResponseEntity<>(reviews, HttpStatus.OK);
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}

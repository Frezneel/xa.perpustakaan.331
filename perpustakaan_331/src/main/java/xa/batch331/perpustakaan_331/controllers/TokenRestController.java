package xa.batch331.perpustakaan_331.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import xa.batch331.perpustakaan_331.models.Token;
import xa.batch331.perpustakaan_331.services.TokenService;

import java.util.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class TokenRestController {

    @Autowired
    private TokenService tokenService;
    private Map<String, Object> result;

    @GetMapping("/token")
    public ResponseEntity<List<Token>> getAllToken(){
        try {
            List<Token> tokens = this.tokenService.getAllToken();
            return new ResponseEntity<>(tokens, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/token/register/result")
    public ResponseEntity<Object> resultTokenRegister(@RequestParam String email, @RequestParam String used_for,
                                                      @RequestParam String token){
        try {
            Optional<Token> token_data = this.tokenService.validationToken(email, used_for, token);
            result = new HashMap<>();
            if (token_data.isPresent()){
                Long date_now = new Date().getTime();
                Long date_expired = token_data.get().getExpired_on().getTime();
                Long is_expired = date_expired - date_now;
                if (is_expired >= 1){
                    result.put("info", "valid");
                    return new ResponseEntity<>(result, HttpStatus.OK);
                }else {
                    result.put("info", "expired");
                    return new ResponseEntity<>(result, HttpStatus.OK);
                }
            }else {
                result.put("info", "invalid");
                return new ResponseEntity<>(result, HttpStatus.OK);
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/token/tokenToExpired")
    public ResponseEntity<Object> tokenToExpired(@RequestParam String email, @RequestParam String used_for){
        try {
            result = new HashMap<>();
            Optional<Token> data = this.tokenService.getLastTokenByEmail(email, used_for);
            if (data.isPresent()){
                data.get().setIs_expired(true);
                data.get().setModified_by(1L);
                data.get().setModified_on(new Date());
                this.tokenService.saveToken(data.get());
                result.put("status", "found");
                result.put("message", "success change to expired");
                return new ResponseEntity<>(result, HttpStatus.OK);
            }else {
                result.put("status", "empty");
                return new ResponseEntity<>(result, HttpStatus.OK);
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/token")
    public ResponseEntity<Object> saveToken(@RequestBody Token token){
        try {
            Date expiredDate = new Date();
            Integer minute = expiredDate.getMinutes();
            expiredDate.setMinutes(minute + 3);

            token.setCreated_on(new Date());
            token.setCreated_by(1L);
            token.setExpired_on(expiredDate);
            this.tokenService.saveToken(token);
            return new ResponseEntity<>(token, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}

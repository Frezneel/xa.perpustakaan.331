package xa.batch331.perpustakaan_331.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import xa.batch331.perpustakaan_331.models.Rak;
import xa.batch331.perpustakaan_331.services.RakService;

import java.util.*;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class RakRestController {

    @Autowired
    private RakService rakService;

    private Map<String, Object> result;

    @GetMapping("/rak")
    public ResponseEntity<List<Rak>> getAllRak(){
        try {
            List<Rak> rakList = this.rakService.getAllRak();
            return new ResponseEntity<>(rakList, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/rak/{id}")
    public ResponseEntity<Optional<Rak>> getRakById(@PathVariable("id")Long id){
        try {
            Optional<Rak> rak = this.rakService.getRakById(id);
            if (rak.isPresent()){
                return new ResponseEntity<>(rak, HttpStatus.OK);
            }else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/rak/{id}")
    public ResponseEntity<?> updateRak(@RequestBody Rak rak, @PathVariable("id")Long id){
        try {
            Optional<Rak> rak_data = this.rakService.getRakById(id);
            Optional<Rak> validasi_lokasi = this.rakService.searchByLokasi(rak.getLokasi().trim());
            result = new HashMap<>();
            if (rak_data.isPresent() && validasi_lokasi.isEmpty()){
                rak.setId(id);
                rak.setCreated_by(rak_data.get().getCreated_by());
                rak.setCreated_on(rak_data.get().getCreated_on());
                rak.setModified_by(1L);
                rak.setModified_on(new Date());
                this.rakService.saveRak(rak);
                result.put("status", "belum");
                result.put("data", rak);
                return new ResponseEntity<>(result, HttpStatus.OK);
            } else if (rak_data.isPresent() && validasi_lokasi.isPresent()) {
                result.put("status", "ada");
                return new ResponseEntity<>(result, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/rak/{id}")
    public ResponseEntity<?> deleteRak (@PathVariable Long id){
        try {
            Optional<Rak> rak = this.rakService.getRakById(id);
            if (rak.isPresent()){
                rak.get().setDeleted_by(1L);
                rak.get().setDeleted_on(new Date());
                rak.get().setIs_delete(true);
                this.rakService.saveRak(rak.get());
                Map<String, Object> result = new HashMap<>();
                result.put("status", "sukses");
                return new ResponseEntity<>(result, HttpStatus.OK);
            }else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/rak")
    public ResponseEntity<Object> addRak(@RequestBody Rak rak){
        try {
            result = new HashMap<>();
            Optional<Rak> data = this.rakService.searchByLokasi(rak.getLokasi().trim());
            if (data.isPresent()){
                result.put("status", "ada");
                return new ResponseEntity<>(result, HttpStatus.OK);
            }else {
                rak.setCreated_on(new Date());
                rak.setCreated_by(1L);
                this.rakService.saveRak(rak);
                result.put("status", "belum");
                result.put("data", rak);
                return new ResponseEntity<>(result, HttpStatus.OK);
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

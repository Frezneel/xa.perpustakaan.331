package xa.batch331.perpustakaan_331.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("rak")
public class RakController {

    @GetMapping
    public ModelAndView index(){
        ModelAndView view = new ModelAndView("rak/index");
        return view;
    }

}

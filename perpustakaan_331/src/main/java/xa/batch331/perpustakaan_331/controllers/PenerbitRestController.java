package xa.batch331.perpustakaan_331.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import xa.batch331.perpustakaan_331.models.Penerbit;
import xa.batch331.perpustakaan_331.services.PenerbitService;

import java.util.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class PenerbitRestController {

    @Autowired
    private PenerbitService penerbitService;
    private Map<String, Object> result;

    @GetMapping("/penerbit")
    public ResponseEntity<List<Penerbit>> getAllPenerbit(){
        try {
            List<Penerbit> penerbitList = this.penerbitService.getAllPenerbit();
            return new ResponseEntity<>(penerbitList, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //pagination
    @GetMapping("/penerbit/map")
    public ResponseEntity<Object> getAllPenerbit(@RequestParam(defaultValue = "1") Integer page,
                                                 @RequestParam(defaultValue = "5") Integer size){
        try {
            Pageable pagingSort = PageRequest.of((page-1), size);
            Page<Penerbit> pageTuts = this.penerbitService.getAllPenerbit(pagingSort);
            List<Penerbit> dataPenerbit = pageTuts.getContent();
            result = new HashMap<>();
            result.put("dataApi", dataPenerbit);
            result.put("halamanSekarang", (pageTuts.getNumber()+1));
            result.put("totalHalaman", pageTuts.getTotalPages());
            result.put("banyakData", pageTuts.getTotalElements());
            return new ResponseEntity<>(result, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Search Pagination
    @GetMapping("/penerbit/result")
    public ResponseEntity<Object> searchQueryPenerbit(@RequestParam String search_query,
                                                      @RequestParam(defaultValue = "1") Integer page,
                                                      @RequestParam(defaultValue = "5") Integer size){
        try {
            Pageable pagingSort = PageRequest.of((page-1), size);
            Page<Penerbit> pageTuts = this.penerbitService.searchQueryPenerbit(search_query, pagingSort);
            List<Penerbit> dataPenerbit = pageTuts.getContent();
            result = new HashMap<>();
            result.put("dataApi", dataPenerbit);
            result.put("halamanSekarang", (pageTuts.getNumber()+1));
            result.put("totalHalaman", pageTuts.getTotalPages());
            result.put("banyakData", pageTuts.getTotalElements());
            return new ResponseEntity<>(result, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/penerbit/{id}")
    public ResponseEntity<Optional<Penerbit>> getPenerbitById(@PathVariable("id")Long id){
        try {
            Optional<Penerbit> penerbit = this.penerbitService.getPenerbitById(id);
            if (penerbit.isPresent()){
                return new ResponseEntity<>(penerbit, HttpStatus.OK);
            }else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/penerbit/{id}")
    public ResponseEntity<?> updatePenerbit(@RequestBody Penerbit penerbit, @PathVariable("id")Long id){
        try {
            Optional<Penerbit> penerbit_data = this.penerbitService.getPenerbitById(id);
            Optional<Penerbit> cek_nama = this.penerbitService.searchByNama(penerbit.getNama().trim());
            result = new HashMap<>();
            if (penerbit_data.isPresent() && cek_nama.isEmpty()){
                result.put("status", "belum");
                penerbit.setId(id);
                penerbit.setCreated_by(penerbit_data.get().getCreated_by());
                penerbit.setCreated_on(penerbit_data.get().getCreated_on());
                penerbit.setModified_by(1L);
                penerbit.setModified_on(new Date());
                this.penerbitService.savePenerbit(penerbit);
                result.put("data", penerbit);
                return new ResponseEntity<>(result, HttpStatus.OK);
            }else if(penerbit_data.isPresent() && cek_nama.isPresent()){
                result.put("status", "ada");
                return new ResponseEntity<>(result, HttpStatus.OK);
            }else{
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/penerbit/{id}")
    public ResponseEntity<?> deletePenerbit (@PathVariable Long id){
        try {
            Optional<Penerbit> penerbit = this.penerbitService.getPenerbitById(id);
            if (penerbit.isPresent()){
                penerbit.get().setDeleted_by(1L);
                penerbit.get().setDeleted_on(new Date());
                penerbit.get().setIs_delete(true);
                this.penerbitService.savePenerbit(penerbit.get());
                Map<String, Object> result = new HashMap<>();
                result.put("status", "sukses");
                return new ResponseEntity<>(result, HttpStatus.OK);
            }else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/penerbit")
    public ResponseEntity<Object> addPenerbit(@RequestBody Penerbit penerbit){
        try {
            Optional<Penerbit> data = this.penerbitService.searchByNama(penerbit.getNama().trim());
            result = new HashMap<>();
            if(data.isPresent()){
                result.put("status", "ada");
                return new ResponseEntity<>(result, HttpStatus.OK);
            }else {
                penerbit.setCreated_on(new Date());
                penerbit.setCreated_by(1L);
                this.penerbitService.savePenerbit(penerbit);
                result.put("status", "belum");
                result.put("data", penerbit);
                return new ResponseEntity<>(result, HttpStatus.OK);
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}

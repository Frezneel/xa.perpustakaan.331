package xa.batch331.perpustakaan_331.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("penerbit")
public class PenerbitController {

    @GetMapping()
    public ModelAndView index(@RequestParam(defaultValue = "1") Integer page,
                              @RequestParam(defaultValue = "5") Integer size){
        ModelAndView view = new ModelAndView("penerbit/index");
        view.addObject("page", page);
        view.addObject("size", size);
        return view;
    }

    @GetMapping("/result")
    public ModelAndView index(@RequestParam String search_query,
                              @RequestParam(defaultValue = "1") Integer page,
                              @RequestParam(defaultValue = "5") Integer size){
        ModelAndView view = new ModelAndView("penerbit/index");
        view.addObject("search_query", search_query);
        view.addObject("page", page);
        view.addObject("size", size);
        return view;
    }
}

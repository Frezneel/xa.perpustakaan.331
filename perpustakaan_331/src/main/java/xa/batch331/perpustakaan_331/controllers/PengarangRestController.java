package xa.batch331.perpustakaan_331.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import xa.batch331.perpustakaan_331.models.Kategori;
import xa.batch331.perpustakaan_331.models.Pengarang;
import xa.batch331.perpustakaan_331.services.PengarangService;

import java.util.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class PengarangRestController {

    @Autowired
    private PengarangService pengarangService;

    private Map<String, Object> result;

    @GetMapping("/pengarang")
    public ResponseEntity<List<Pengarang>> getAllPengarang(){
        try {
            List<Pengarang> pengarangList = this.pengarangService.getAllPengarang();
            return new ResponseEntity<>(pengarangList, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/pengarang/map")
    public ResponseEntity<Object> getAllPengarang(@RequestParam(defaultValue = "1") Integer page,
                                                  @RequestParam(defaultValue = "5") Integer size){
        try {
            Pageable pageable = PageRequest.of((page-1), size);
            Page<Pengarang> pageTuts = this.pengarangService.getAllPengarang(pageable);
            List<Pengarang> dataPengarang = pageTuts.getContent();

            result = new HashMap<>();
            result.put("dataApi", dataPengarang);
            result.put("halamanSekarang", (pageTuts.getNumber()+1));
            result.put("totalHalaman", pageTuts.getTotalPages());
            result.put("banyakData", pageTuts.getTotalElements());
            return new ResponseEntity<>(result, HttpStatus.OK);

        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/pengarang/result")
    public ResponseEntity<Object> searchQueryPengarang(@RequestParam String search_query,
                                                       @RequestParam(defaultValue = "1") Integer page,
                                                       @RequestParam(defaultValue = "5") Integer size){
        try {
            Pageable pageable = PageRequest.of((page-1), size);
            Page<Pengarang> pageTuts = this.pengarangService.searchQueryPengarang(pageable, search_query);
            List<Pengarang> dataPengarang = pageTuts.getContent();

            result = new HashMap<>();
            result.put("dataApi", dataPengarang);
            result.put("halamanSekarang", (pageTuts.getNumber()+1));
            result.put("totalHalaman", pageTuts.getTotalPages());
            result.put("banyakData", pageTuts.getTotalElements());
            return new ResponseEntity<>(result, HttpStatus.OK);

        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }



    @GetMapping("/pengarang/{id}")
    public ResponseEntity<Optional<Pengarang>> getPengarangById(@PathVariable("id")Long id){
        try {
            Optional<Pengarang> pengarang = this.pengarangService.getPengarangById(id);
            if (pengarang.isPresent()){
                return new ResponseEntity<>(pengarang, HttpStatus.OK);
            }else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/pengarang/{id}")
    public ResponseEntity<?> updatePengarang(@RequestBody Pengarang pengarang, @PathVariable("id")Long id){
        try {
            Optional<Pengarang> pengarang_data = this.pengarangService.getPengarangById(id);
            Optional<Pengarang> validasi_nama = this.pengarangService.searchByNama(pengarang.getNama().trim());
            result = new HashMap<>();
            if (pengarang_data.isPresent() && validasi_nama.isEmpty()){
                pengarang.setId(id);
                pengarang.setCreated_by(pengarang_data.get().getCreated_by());
                pengarang.setCreated_on(pengarang_data.get().getCreated_on());
                pengarang.setModified_by(1L);
                pengarang.setModified_on(new Date());
                this.pengarangService.savePengarang(pengarang);
                result.put("status","belum");
                result.put("data", pengarang);
                return new ResponseEntity<>(pengarang, HttpStatus.OK);
            } else if (pengarang_data.isPresent() && validasi_nama.isPresent()) {
                result.put("status","ada");
                return new ResponseEntity<>(result, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/pengarang/{id}")
    public ResponseEntity<?> deletePengarang (@PathVariable Long id){
        try {
            Optional<Pengarang> pengarang = this.pengarangService.getPengarangById(id);
            if (pengarang.isPresent()){
                pengarang.get().setDeleted_by(1L);
                pengarang.get().setDeleted_on(new Date());
                pengarang.get().setIs_delete(true);
                this.pengarangService.savePengarang(pengarang.get());
                Map<String, Object> result = new HashMap<>();
                result.put("status", "sukses");
                return new ResponseEntity<>(result, HttpStatus.OK);
            }else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/pengarang")
    public ResponseEntity<Object> addPengarang(@RequestBody Pengarang pengarang){
        try {
            result = new HashMap<>();
            Optional<Pengarang> data = this.pengarangService.searchByNama(pengarang.getNama().trim());
            if (data.isPresent()){
                result.put("status", "ada");
                return new ResponseEntity<>(result, HttpStatus.OK);
            }else {
                pengarang.setCreated_on(new Date());
                pengarang.setCreated_by(1L);
                this.pengarangService.savePengarang(pengarang);
                result.put("status", "belum");
                result.put("data", pengarang);
                return new ResponseEntity<>(result, HttpStatus.OK);
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

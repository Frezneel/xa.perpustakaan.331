package xa.batch331.perpustakaan_331.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.persistence.*;

import java.util.Date;

@Entity
@Table(name = "m_user")
public class M_User extends BaseProperties{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "biodata_id", insertable = false, updatable = false)
    private Biodata biodata;
    @Column(name = "biodata_id", nullable = false)
    private Long biodata_id;

    @ManyToOne
    @JoinColumn(name = "role_id", insertable = false, updatable = false)
    private M_Role m_role;
    @Column(name = "role_id")
    private Long role_id;

    @Column(name = "email", nullable = false)
    private String email;
    @Column(name = "password", nullable = false)
    private String password;
    @Column(name = "login_attempt")
    private Integer login_attempt;
    @Column(name = "is_locked")
    private Boolean is_locked;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "last_login")
    private Date last_login;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Biodata getBiodata() {
        return biodata;
    }

    public void setBiodata(Biodata biodata) {
        this.biodata = biodata;
    }

    public Long getBiodata_id() {
        return biodata_id;
    }

    public void setBiodata_id(Long biodata_id) {
        this.biodata_id = biodata_id;
    }

    public M_Role getM_role() {
        return m_role;
    }

    public void setM_role(M_Role m_role) {
        this.m_role = m_role;
    }

    public Long getRole_id() {
        return role_id;
    }

    public void setRole_id(Long role_id) {
        this.role_id = role_id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getLogin_attempt() {
        return login_attempt;
    }

    public void setLogin_attempt(Integer login_attempt) {
        this.login_attempt = login_attempt;
    }

    public Boolean getIs_locked() {
        return is_locked;
    }

    public void setIs_locked(Boolean is_locked) {
        this.is_locked = is_locked;
    }

    public Date getLast_login() {
        return last_login;
    }

    public void setLast_login(Date last_login) {
        this.last_login = last_login;
    }
}

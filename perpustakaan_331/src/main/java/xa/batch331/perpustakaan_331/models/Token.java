package xa.batch331.perpustakaan_331.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.persistence.*;

import java.util.Date;

@Entity
@Table(name = "token")
public class Token extends BaseProperties{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "email", nullable = false)
    private String email;

    @ManyToOne
    @JoinColumn(name = "user_id", insertable = false, updatable = false)
    private M_User m_user;

    @Column(name = "user_id", nullable = false)
    private Long user_id;

    @Column(name = "token", nullable = false)
    private String token;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "expired_on", nullable = false)
    private Date expired_on;

    @Column(name = "is_expired")
    private Boolean is_expired;

    @Column(name = "used_for", nullable = false)
    private String used_for;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public M_User getM_user() {
        return m_user;
    }

    public void setM_user(M_User m_user) {
        this.m_user = m_user;
    }

    public Long getUser_id() {
        return user_id;
    }

    public void setUser_id(Long user_id) {
        this.user_id = user_id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Date getExpired_on() {
        return expired_on;
    }

    public void setExpired_on(Date expired_on) {
        this.expired_on = expired_on;
    }

    public Boolean getIs_expired() {
        return is_expired;
    }

    public void setIs_expired(Boolean is_expired) {
        this.is_expired = is_expired;
    }

    public String getUsed_for() {
        return used_for;
    }

    public void setUsed_for(String used_for) {
        this.used_for = used_for;
    }
}

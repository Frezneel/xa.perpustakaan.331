package xa.batch331.perpustakaan_331.models;

import jakarta.persistence.*;


@Entity
@Table(name = "kategori_buku")
public class KategoriBuku extends BaseProperties{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "buku_id", insertable = false, updatable = false)
    private Buku buku;

    @Column(name = "buku_id", nullable = false)
    private Long buku_id;

    @ManyToOne
    @JoinColumn(name = "kategori_id", insertable = false, updatable = false)
    private Kategori kategori;

    @Column(name = "kategori_id", nullable = false)
    private Long kategori_id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Buku getBuku() {
        return buku;
    }

    public void setBuku(Buku buku) {
        this.buku = buku;
    }

    public Long getBuku_id() {
        return buku_id;
    }

    public void setBuku_id(Long buku_id) {
        this.buku_id = buku_id;
    }

    public Kategori getKategori() {
        return kategori;
    }

    public void setKategori(Kategori kategori) {
        this.kategori = kategori;
    }

    public Long getKategori_id() {
        return kategori_id;
    }

    public void setKategori_id(Long kategori_id) {
        this.kategori_id = kategori_id;
    }
}

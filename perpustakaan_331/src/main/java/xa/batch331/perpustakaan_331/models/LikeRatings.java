package xa.batch331.perpustakaan_331.models;

import jakarta.persistence.*;

@Entity
@Table(name = "like_ratings")
public class LikeRatings extends BaseProperties{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "buku_id",insertable = false, updatable = false)
    private Buku buku;

    @Column(name = "buku_id")
    private Long buku_id;

    @Column(name = "rating")
    private Double rating;

    @Column(name = "liked")
    private Boolean liked;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Buku getBuku() {
        return buku;
    }

    public void setBuku(Buku buku) {
        this.buku = buku;
    }

    public Long getBuku_id() {
        return buku_id;
    }

    public void setBuku_id(Long buku_id) {
        this.buku_id = buku_id;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public Boolean getLiked() {
        return liked;
    }

    public void setLiked(Boolean liked) {
        this.liked = liked;
    }
}

package xa.batch331.perpustakaan_331.models;

import jakarta.persistence.*;

@Entity
@Table(name = "rak")
public class Rak extends BaseProperties{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "lokasi", nullable = false)
    private String lokasi;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLokasi() {
        return lokasi;
    }

    public void setLokasi(String lokasi) {
        this.lokasi = lokasi;
    }
}

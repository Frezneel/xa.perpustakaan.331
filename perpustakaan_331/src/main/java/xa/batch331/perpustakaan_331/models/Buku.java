package xa.batch331.perpustakaan_331.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.persistence.*;

import java.util.Date;
import java.util.List;

@Entity
@Table(name = "buku")
public class Buku extends BaseProperties{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "judul", nullable = false)
    private String judul;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy")
    @Column(name = "tahun_terbit")
    private Date tahun_terbit;

    @Column(name = "jumlah_buku")
    private Long jumlah_buku;

    @Column(name = "jumlah_halaman")
    private Long jumlah_halaman;

    @Column(name = "isbn")
    private String isbn;

    @ManyToOne
    @JoinColumn(name = "pengarang_id",insertable = false, updatable = false)
    private Pengarang pengarang;

    @Column(name = "pengarang_id")
    private Long pengarang_id;

    @ManyToOne
    @JoinColumn(name = "penerbit_id", insertable = false, updatable = false)
    private Penerbit penerbit;

    @Column(name = "penerbit_id")
    private Long penerbit_id;

    @ManyToOne
    @JoinColumn(name = "rak_id", insertable = false, updatable = false)
    private Rak rak;

    @Column(name = "rak_id")
    private Long rak_id;

    @Column(name = "image_file")
    private byte[] image_file;
    @Column(name = "image_path")
    private String image_path;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public Date getTahun_terbit() {
        return tahun_terbit;
    }

    public void setTahun_terbit(Date tahun_terbit) {
        this.tahun_terbit = tahun_terbit;
    }

    public Long getJumlah_buku() {
        return jumlah_buku;
    }

    public void setJumlah_buku(Long jumlah_buku) {
        this.jumlah_buku = jumlah_buku;
    }

    public Long getJumlah_halaman() {
        return jumlah_halaman;
    }

    public void setJumlah_halaman(Long jumlah_halaman) {
        this.jumlah_halaman = jumlah_halaman;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public Pengarang getPengarang() {
        return pengarang;
    }

    public void setPengarang(Pengarang pengarang) {
        this.pengarang = pengarang;
    }

    public Long getPengarang_id() {
        return pengarang_id;
    }

    public void setPengarang_id(Long pengarang_id) {
        this.pengarang_id = pengarang_id;
    }

    public Penerbit getPenerbit() {
        return penerbit;
    }

    public void setPenerbit(Penerbit penerbit) {
        this.penerbit = penerbit;
    }

    public Long getPenerbit_id() {
        return penerbit_id;
    }

    public void setPenerbit_id(Long penerbit_id) {
        this.penerbit_id = penerbit_id;
    }

    public Rak getRak() {
        return rak;
    }

    public void setRak(Rak rak) {
        this.rak = rak;
    }

    public Long getRak_id() {
        return rak_id;
    }

    public void setRak_id(Long rak_id) {
        this.rak_id = rak_id;
    }

    public byte[] getImage_file() {
        return image_file;
    }

    public void setImage_file(byte[] image_file) {
        this.image_file = image_file;
    }

    public String getImage_path() {
        return image_path;
    }

    public void setImage_path(String image_path) {
        this.image_path = image_path;
    }
}

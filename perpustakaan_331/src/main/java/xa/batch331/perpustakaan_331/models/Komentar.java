package xa.batch331.perpustakaan_331.models;

import jakarta.persistence.*;

@Entity
@Table(name = "komentar")
public class Komentar extends BaseProperties{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "parent_id", insertable = false, updatable = false)
    private Komentar komentar;

    @Column(name = "parent_id")
    private Long parent_id;

    @ManyToOne
    @JoinColumn(name = "buku_id", insertable = false, updatable = false)
    private Buku buku;

    @Column(name = "buku_id")
    private Long buku_id;

    @Column(name = "content")
    private String content;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Komentar getKomentar() {
        return komentar;
    }

    public void setKomentar(Komentar komentar) {
        this.komentar = komentar;
    }

    public Long getParent_id() {
        return parent_id;
    }

    public void setParent_id(Long parent_id) {
        this.parent_id = parent_id;
    }

    public Buku getBuku() {
        return buku;
    }

    public void setBuku(Buku buku) {
        this.buku = buku;
    }

    public Long getBuku_id() {
        return buku_id;
    }

    public void setBuku_id(Long buku_id) {
        this.buku_id = buku_id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
